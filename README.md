# Overview

The nxlog-raijin-datasource plugin provides connectivity to the Raijin Database

Raijin is a schemaless columnar database with native support for compression and 
encryption - and SQL query syntax.

Get the database at https://raijin.co/ or via https://hub.docker.com/u/raijindb

