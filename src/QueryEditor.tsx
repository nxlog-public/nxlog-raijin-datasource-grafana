import React, { FormEvent, PureComponent } from 'react';
import { Alert, Button, Field, Switch, Tab, TabContent, TabsBar, TextArea } from '@grafana/ui';
import { QueryEditorProps } from '@grafana/data';
import { config } from '@grafana/runtime';
import { DataSource } from './datasource';
import { MyDataSourceOptions, MyQuery } from './types';
import { GetLastQueryError } from './common';
import { QueryBuilder } from './query-editor';
import { SelectStatement } from './query-editor/sql-statement';

type Props = QueryEditorProps<DataSource, MyQuery, MyDataSourceOptions>;

enum QueryEditorTab {
  TAB_QUERY_BUILDER = 0,
  TAB_QUERY_MANUAL_EDITOR = 1,
}

interface QueryEditorState {
  tabIndex: QueryEditorTab;
}

export class QueryEditor extends PureComponent<Props, QueryEditorState> {
  private selectStatement: SelectStatement;

  constructor(props: any) {
    super(props);
    const { query } = this.props;
    this.state = {
      tabIndex: query.manualMode
        ? QueryEditorTab.TAB_QUERY_MANUAL_EDITOR
        : QueryEditorTab.TAB_QUERY_BUILDER,
    };
    this.selectStatement = SelectStatement.deserialize(this.props.query.builderQuerySerialized);
  }

  override componentWillUnmount() {
    this.selectStatement.tearDown();
  }

  override render() {
    // NOTE: just to remember, the current time range here can be accessed by:
    //       this.props.data?.timeRange - this used to be passed to sql generation func
    return (
      <div style={{ display: 'grid' }}>
        <TabsBar>
          <Tab
            disabled={this.props.query.manualMode}
            label="Query builder"
            active={this.state.tabIndex === QueryEditorTab.TAB_QUERY_BUILDER}
            onChangeTab={() => {
              if (!this.props.query.manualMode) {
                this.setState({ ...this.state, tabIndex: QueryEditorTab.TAB_QUERY_BUILDER });
              }
            }}
          />
          <Tab
            label="Query text editor"
            active={this.state.tabIndex === QueryEditorTab.TAB_QUERY_MANUAL_EDITOR}
            onChangeTab={() => {
              this.setState({ ...this.state, tabIndex: QueryEditorTab.TAB_QUERY_MANUAL_EDITOR });
            }}
          />
        </TabsBar>

        {this.state.tabIndex === QueryEditorTab.TAB_QUERY_BUILDER && (
          <TabContent>
            <QueryBuilder
              datasource={this.props.datasource}
              selectStatement={this.selectStatement}
              onChange={() => {
                const { onChange, query } = this.props;
                onChange({
                  ...query,
                  builderQuerySerialized: this.selectStatement.serialize(),
                });
              }}
              onQueryRun={() => {
                this.props.onRunQuery();
              }}
            />
          </TabContent>
        )}

        {this.state.tabIndex === QueryEditorTab.TAB_QUERY_MANUAL_EDITOR && (
          <TabContent>
            <div
              style={{
                display: 'grid',
                rowGap: '16px',
                padding: config.theme2.spacing(0.5, 0.5),
              }}
            >
              <div style={{ display: 'flex', gap: '10px' }}>
                <Field label="Edit manually">
                  <Switch
                    value={this.props.query.manualMode}
                    onChange={(event: FormEvent<HTMLInputElement>) => {
                      let { manualQueryText } = this.props.query;
                      const { onChange } = this.props;
                      const isManualMode = event.currentTarget.checked;
                      if (
                        isManualMode &&
                        (manualQueryText === undefined || manualQueryText === '')
                      ) {
                        manualQueryText = this.selectStatement.getSql();
                      }
                      onChange({
                        ...this.props.query,
                        manualMode: isManualMode,
                        manualQueryText: manualQueryText,
                      });
                    }}
                  />
                </Field>
                <Field label="Autogenerate time column" disabled={!this.props.query.manualMode}>
                  <Switch
                    value={this.props.query.manualModeAutoTimeField && this.props.query.manualMode}
                    onChange={(event: FormEvent<HTMLInputElement>) => {
                      const { onChange, onRunQuery } = this.props;
                      onChange({
                        ...this.props.query,
                        manualModeAutoTimeField: event.currentTarget.checked,
                      });
                      onRunQuery();
                    }}
                  />
                </Field>
              </div>

              {GetLastQueryError() !== '' && <Alert title={GetLastQueryError()} severity="error" />}

              <TextArea
                style={{ height: 180 }}
                readOnly={!this.props.query.manualMode}
                onChange={(event: FormEvent<HTMLTextAreaElement>) => {
                  const { onChange } = this.props;
                  onChange({
                    ...this.props.query,
                    manualQueryText: event.currentTarget.value,
                  });
                }}
                value={this.props.query.manualQueryText}
              />

              <div style={{ display: 'flex', gap: '10px' }}>
                <Button
                  disabled={!this.props.query.manualMode}
                  onClick={() => {
                    const { onRunQuery } = this.props;
                    onRunQuery();
                  }}
                >
                  Apply
                </Button>
                <Button
                  disabled={!this.props.query.manualMode}
                  onClick={() => {
                    const { onChange, onRunQuery } = this.props;
                    onChange({
                      ...this.props.query,
                      manualQueryText: this.selectStatement.getSql(),
                    });
                    onRunQuery();
                  }}
                >
                  Copy auto-generated query
                </Button>
              </div>
            </div>
          </TabContent>
        )}
      </div>
    );
  }
}
