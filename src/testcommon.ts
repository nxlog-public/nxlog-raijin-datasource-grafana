import { DataSourceInstanceSettings } from '@grafana/data';
import { MyDataSourceOptions } from './types';
import { FetchResponse } from '@grafana/runtime';
import { BackendSrvRequest } from '@grafana/runtime/services/backendSrv';
import { Observable, of } from 'rxjs';

export const fetchMock = jest.fn();

export function doMockExpectedResponse(response: any, status = 200, statusText = '') {
  fetchMock.mockImplementation((_options: BackendSrvRequest): Observable<FetchResponse> => {
    return of(createRaijinServerResponse(response, status, statusText));
  });
}

export const GRAFANA_API_URL = 'proxied';
export const RAIJIN_URL = 'http://raijin.org:2500';

export function createDatasourceSettings(
  raijinUrl: string
): DataSourceInstanceSettings<MyDataSourceOptions> {
  return {
    url: GRAFANA_API_URL,
    id: 1,
    directUrl: 'direct',
    user: 'test',
    password: 'test',
    jsonData: {
      directUrl: raijinUrl,
    } as any,
  } as unknown as DataSourceInstanceSettings<MyDataSourceOptions>;
}

export function createRaijinServerResponse(
  response: any,
  status = 200,
  statusText = ''
): FetchResponse {
  return {
    data: response,
    config: {
      url: RAIJIN_URL,
    },
    headers: new Headers(),
    ok: true,
    redirected: false,
    status: status,
    statusText: statusText,
    type: 'default',
    url: '',
  };
}
