import React, { PureComponent } from 'react';
import { Button, Tooltip } from '@grafana/ui';
import { css } from '@emotion/css';
import { config } from '@grafana/runtime';
import { Newable } from '../../common';
import { HeaderButton } from './Header';

type Properties<Props, State, Component extends PureComponent<Props, State>> = {
  itemsProps: Props[];
  itemType: Newable<Component>;
  addButton: {
    text?: string;
    tooltip: string;
  };
  onAddButtonClicked: () => void;
  onRemoveButtonClicked: (index: number) => void;
};

export class ComponentsArray<P, S, C extends PureComponent<P, S>> extends PureComponent<
  Properties<P, S, C>
> {
  private static readonly styles = {
    main: css({
      display: 'grid',
      gridTemplateColumns: '1fr',
      gridTemplateRows: '1fr auto',
    }),
    componentsArea: css({
      display: 'flex',
      flexWrap: 'wrap',
      gap: '16px',
    }),
    item: css({
      background: config.theme2.colors.background.primary,
      padding: config.theme2.spacing(0.5, 0.5, 0.5, 1),
      borderBottom: `1px solid ${config.theme2.colors.border.medium}`,
      borderTop: `1px solid ${config.theme2.colors.border.medium}`,
      borderLeft: `1px solid ${config.theme2.colors.border.medium}`,
      borderRight: `1px solid ${config.theme2.colors.border.medium}`,
    }),
    addButton: css({
      padding: config.theme2.spacing(0.5, 0.5, 0.5, 0),
    }),
    grow: css({
      flexGrow: 1,
    }),
  };

  override render() {
    const styles = ComponentsArray.styles;
    return (
      <div className={`${styles.main}`}>
        <div className={`${styles.componentsArea}`}>
          {this.props.itemsProps.map((itemProps, index) => {
            return (
              <div className={`${styles.item}`} key={index}>
                <div style={{ display: 'flex', gap: '5px' }}>
                  <this.props.itemType {...itemProps} />
                  <HeaderButton
                    icon={'times'}
                    hint={'Remove item'}
                    onClick={() => {
                      // TODO: consider adding a confirmation
                      this.props.onRemoveButtonClicked(index);
                    }}
                  />
                </div>
              </div>
            );
          })}
        </div>
        <div className={`${styles.addButton}`}>
          <Tooltip content={this.props.addButton.tooltip} placement="top">
            <Button
              size="xs"
              variant="secondary"
              icon="plus"
              onClick={() => {
                this.props.onAddButtonClicked();
              }}
            >
              {this.props.addButton.text}
            </Button>
          </Tooltip>
        </div>
      </div>
    );
  }
}
