import React, { PureComponent } from 'react';
import { css } from '@emotion/css';
import { Header, HeaderButtonList } from './Header';
import { Newable } from '../../common';

interface VariantFieldOption<Props, Component extends PureComponent<Props>> {
  component: Newable<Component>;
  props: Props;
  title: string;
  menuLabel: string;
}

interface VariantFieldProps {
  components: Array<VariantFieldOption<any, any>>;
  selectedIndex: number;
  additionalHeaderButtons?: HeaderButtonList;
  onSelectedIndexChanged: (index: number) => void;
}

export class VariantField extends PureComponent<VariantFieldProps> {
  private readonly styles = {
    grid: css({
      display: 'grid',
      gridTemplateColumns: '1fr',
      gridTemplateRows: '2fr',
    }),
    field: css({
      display: 'flex',
    }),
    grow: css({
      flexGrow: 1,
    }),
  };

  constructor(props: any) {
    super(props);
    if (this.props.components.length === 0) {
      throw Error('No components provided for VariantField');
    }
  }

  override render() {
    return (
      <div className={`${this.styles.grid}`}>
        <Header
          title={this.props.components[this.props.selectedIndex]!.title}
          buttons={[
            {
              button: { icon: 'angle-down', hint: 'Change component type' },
              items: this.props.components.map((option, index) => ({
                label: option.menuLabel,
                onClick: () => {
                  this.props.onSelectedIndexChanged(index);
                },
              })),
              checkedOption: this.props.selectedIndex,
            },
            ...(this.props.additionalHeaderButtons || []),
          ]}
        />
        <div className={`${this.styles.field}`}>
          <div className={`${this.styles.grow}`}>
            {(() => {
              const index = this.props.selectedIndex;
              const option = this.props.components[index]!;
              return <option.component {...option.props} key={index} />;
            })()}
          </div>
        </div>
      </div>
    );
  }
}
