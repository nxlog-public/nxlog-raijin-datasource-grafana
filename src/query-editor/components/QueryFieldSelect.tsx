import React, { PureComponent } from 'react';
import { Select } from '@grafana/ui';
import { SelectableValue } from '@grafana/data';
import * as sqlStatement from '../sql-statement';

export enum QueryFieldSelectMode {
  showAllField,
  showAggregatedFieldsOnly,
  showNonAggregatedFieldsOnly,
}

interface QueryFieldSelectProps {
  value?: sqlStatement.Column;
  selectStatementColumns?: sqlStatement.ColumnList;
  mode: QueryFieldSelectMode;
  onChange: (value: sqlStatement.Column) => void;
}

export class QueryFieldSelect extends PureComponent<QueryFieldSelectProps> {
  override render() {
    return (
      <div>
        <Select<sqlStatement.Column>
          options={this.props.selectStatementColumns
            ?.filter((column) => {
              if (column.hasDbField() && column.dbField!.isAsterisk()) {
                return false;
              }
              switch (this.props.mode) {
                case QueryFieldSelectMode.showAllField:
                  return true;
                case QueryFieldSelectMode.showAggregatedFieldsOnly:
                  return column.hasAggregation();
                case QueryFieldSelectMode.showNonAggregatedFieldsOnly:
                  return !column.hasAggregation();
                default:
                  throw new Error(`Unexpected QueryFieldSelectMode: ${this.props.mode}`);
              }
            })
            .map((column) => ({ label: column.getSql(), value: column }))}
          value={{ label: this.props.value?.getSql(), value: this.props.value }}
          onChange={(value: SelectableValue<sqlStatement.Column>) => {
            this.props.onChange(value.value!);
          }}
        />
      </div>
    );
  }
}
