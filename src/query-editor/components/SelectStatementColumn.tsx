import React, { FormEvent, PureComponent } from 'react';
import { AggregationKind, AggregationSelect } from './AggregationSelect';
import { VariantField } from './Variant';
import { FieldSelect } from './FieldSelect';
import { ExpressionInput, escapeStringValue, unescapeStringValue } from './ExpressionInput';
import { WithHeader } from './Header';
import { Input } from '@grafana/ui';
import { CommonProps } from './types';
import { DbField } from '../../DatabaseClient';
import * as sqlStatement from '../sql-statement';

export interface SelectStatementColumnProps extends CommonProps {
  value: sqlStatement.Column;
  onChange: (value: sqlStatement.Column) => void;
}

export interface SelectStatementColumnState {
  hasAggregation: boolean;
  hasAlias: boolean;
}

enum SelectStatementColumnVariant {
  FIELD = 0,
  MANUAL_EXPRESSION = 1,
}

export class SelectStatementColumn extends PureComponent<
  SelectStatementColumnProps,
  SelectStatementColumnState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      hasAggregation: this.props.value.hasAggregation(),
      hasAlias: this.props.value.hasAlias(),
    };
  }

  addAggregation() {
    this.setState({ ...this.state, hasAggregation: true });
  }

  addAlias() {
    this.setState({ ...this.state, hasAlias: true });
  }

  removeAggregation() {
    this.setState({ ...this.state, hasAggregation: false });
    const { value } = this.props;
    value.aggregation = undefined;
    this.props.onChange(value);
  }

  removeAlias() {
    this.setState({ ...this.state, hasAlias: false });
    const { value } = this.props;
    value.alias = '';
    this.props.onChange(value);
  }

  override render() {
    return (
      <div style={{ display: 'flex' }}>
        {this.state.hasAggregation && (
          <div>
            <WithHeader
              title={'Agg'}
              buttons={[
                {
                  icon: 'times',
                  hint: 'Remove aggregation',
                  onClick: () => this.removeAggregation(),
                },
              ]}
            >
              <AggregationSelect
                value={this.props.value.aggregation}
                onChange={(aggregation: AggregationKind) => {
                  const { value } = this.props;
                  value.aggregation = aggregation;
                  this.props.onChange(value);
                }}
              />
            </WithHeader>
          </div>
        )}
        <div>
          <VariantField
            components={[
              {
                // SelectStatementColumnVariant.FIELD
                component: FieldSelect,
                props: {
                  datasource: this.props.datasource,
                  database: this.props.database,
                  table: this.props.table,
                  forceUpdateState: this.props.forceUpdateState,
                  key: this.props.table,
                  value: this.props.value.dbField,
                  allowAsterisk: true,
                  onChange: (field: DbField) => {
                    let { value } = this.props;
                    value.dbField = field;
                    this.props.onChange(value);
                  },
                },
                title: 'Table field',
                menuLabel: 'Table field',
              },
              {
                // SelectStatementColumnVariant.MANUAL_EXPRESSION
                component: ExpressionInput,
                props: {
                  value: this.props.value.manualExpression,
                  onChange: (expression: string) => {
                    const { value } = this.props;
                    value.manualExpression = expression;
                    this.props.onChange(value);
                  },
                },
                title: 'Manual expression',
                menuLabel: 'Manual expression',
              },
            ]}
            additionalHeaderButtons={[
              {
                button: { icon: 'plus', hint: 'Add aggregation/alias' },
                items: [
                  {
                    label: 'Add aggregation',
                    onClick: () => this.addAggregation(),
                  },
                  {
                    label: 'Add alias',
                    onClick: () => this.addAlias(),
                  },
                ],
              },
            ]}
            selectedIndex={
              this.props.value.hasDbField()
                ? SelectStatementColumnVariant.FIELD
                : SelectStatementColumnVariant.MANUAL_EXPRESSION
            }
            onSelectedIndexChanged={(index: number) => {
              const { value } = this.props;
              switch (index) {
                case SelectStatementColumnVariant.FIELD:
                  value.dbField = new DbField();
                  break;
                case SelectStatementColumnVariant.MANUAL_EXPRESSION:
                  value.manualExpression = '';
                  break;
                default:
                  throw new Error('wrong index');
              }
              this.props.onChange(value);
            }}
          />
        </div>
        {this.state.hasAlias && (
          <div>
            <WithHeader
              title={'Alias'}
              buttons={[
                {
                  icon: 'times',
                  hint: 'Remove alias',
                  onClick: () => this.removeAlias(),
                },
              ]}
            >
              <Input
                placeholder="Enter the alias"
                value={unescapeStringValue(this.props.value?.alias)}
                onChange={(eventValue: FormEvent<HTMLInputElement>) => {
                  const { value } = this.props;
                  value.alias = escapeStringValue(eventValue.currentTarget.value);
                  this.props.onChange(value);
                }}
              />
            </WithHeader>
          </div>
        )}
      </div>
    );
  }
}
