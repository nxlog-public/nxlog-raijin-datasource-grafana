import React, { PureComponent } from 'react';
import { VariantField } from './Variant';
import { BooleanOperationSelect } from './BooleanOperationSelect';
import { CommonProps } from './types';
import { ExpressionInput } from './ExpressionInput';
import { QueryFieldSelect, QueryFieldSelectMode } from './QueryFieldSelect';
import * as sqlStatement from '../sql-statement';

interface HavingExpressionArgumentProps {
  value?: sqlStatement.HavingExpressionArgument;
  selectStatementColumns?: sqlStatement.ColumnList;
  onChange: (value: sqlStatement.HavingExpressionArgument) => void;
}

enum HavingExpressionArgumentVariant {
  SELECT_STATEMENT_COLUMN = 0,
  MANUAL_EXPRESSION = 1,
}

class HavingExpressionArgument extends PureComponent<HavingExpressionArgumentProps> {
  private ensureExpressionCreated(
    value?: sqlStatement.HavingExpressionArgument
  ): sqlStatement.HavingExpressionArgument {
    if (value !== undefined) {
      return value;
    }
    return new sqlStatement.HavingExpressionArgument();
  }
  private static getVariantIndexByValue(
    value?: sqlStatement.HavingExpressionArgument
  ): HavingExpressionArgumentVariant {
    if (value === undefined || value.hasSelectStatementColumn()) {
      return HavingExpressionArgumentVariant.SELECT_STATEMENT_COLUMN;
    } else {
      return HavingExpressionArgumentVariant.MANUAL_EXPRESSION;
    }
  }
  override render() {
    return (
      <VariantField
        components={[
          {
            component: QueryFieldSelect,
            props: {
              value: this.props.value?.selectStatementColumn,
              selectStatementColumns: this.props.selectStatementColumns,
              mode: QueryFieldSelectMode.showAggregatedFieldsOnly,
              onChange: (selectStatementColumn: sqlStatement.Column) => {
                let value = this.ensureExpressionCreated(this.props.value);
                value.selectStatementColumn = selectStatementColumn;
                this.props.onChange(value);
              },
            },
            title: 'Query field',
            menuLabel: 'Query field (from SELECT statement)',
          },
          {
            component: ExpressionInput,
            props: {
              value: this.props.value?.manualExpression,
              onChange: (expression: string) => {
                let value = this.ensureExpressionCreated(this.props.value);
                value.manualExpression = expression;
                this.props.onChange(value);
              },
            },
            title: 'Manual expression',
            menuLabel: 'Manual expression',
          },
        ]}
        selectedIndex={HavingExpressionArgument.getVariantIndexByValue(this.props.value)}
        onSelectedIndexChanged={(index: number) => {
          let value = this.ensureExpressionCreated(this.props.value);
          switch (index) {
            case HavingExpressionArgumentVariant.SELECT_STATEMENT_COLUMN:
              value.selectStatementColumn = new sqlStatement.Column();
              break;
            case HavingExpressionArgumentVariant.MANUAL_EXPRESSION:
              value.manualExpression = '';
              break;
            default:
              throw new Error('wrong index');
          }
          this.props.onChange(value);
        }}
      />
    );
  }
}

export interface HavingExpressionProps extends CommonProps {
  value?: sqlStatement.HavingExpression;
  selectStatementColumns?: sqlStatement.ColumnList;
  onChange: (value: sqlStatement.HavingExpression) => void;
}

export class HavingExpression extends PureComponent<HavingExpressionProps> {
  private ensureHavingExpression(
    value?: sqlStatement.HavingExpression
  ): sqlStatement.HavingExpression {
    if (value !== undefined) {
      return value;
    }
    return new sqlStatement.HavingExpression();
  }

  override render() {
    return (
      <div style={{ display: 'flex' }}>
        <div>
          <HavingExpressionArgument
            value={this.props.value?.leftArgument}
            selectStatementColumns={this.props.selectStatementColumns}
            onChange={(havingArgument: sqlStatement.HavingExpressionArgument) => {
              let value = this.ensureHavingExpression(this.props.value);
              value.leftArgument = havingArgument;
              this.props.onChange(value);
            }}
          />
        </div>
        <div style={{ marginTop: 30 }}>
          <BooleanOperationSelect
            value={this.props.value?.operation}
            onChange={(operation: sqlStatement.BoolOperation) => {
              let value = this.ensureHavingExpression(this.props.value);
              value.operation = operation;
              this.props.onChange(value);
            }}
          />
        </div>
        <div>
          <HavingExpressionArgument
            value={this.props.value?.rightArgument}
            selectStatementColumns={this.props.selectStatementColumns}
            onChange={(havingArgument: sqlStatement.HavingExpressionArgument) => {
              let value = this.ensureHavingExpression(this.props.value);
              value.rightArgument = havingArgument;
              this.props.onChange(value);
            }}
          />
        </div>
      </div>
    );
  }
}
