import React, { PureComponent } from 'react';
import { VariantField } from './Variant';
import { CommonProps } from './types';
import { FieldSelect } from './FieldSelect';
import { ExpressionInput } from './ExpressionInput';
import { QueryFieldSelect, QueryFieldSelectMode } from './QueryFieldSelect';
import { DbField } from '../../DatabaseClient';
import * as sqlStatement from '../sql-statement';

export interface GroupByExpressionProps extends CommonProps {
  value?: sqlStatement.GroupByExpression;
  selectStatementColumns?: sqlStatement.ColumnList;
  onChange: (value: sqlStatement.GroupByExpression) => void;
}

enum GroupByExpressionVariant {
  SELECT_STATEMENT_COLUMN = 0,
  TABLE_FIELD = 1,
  MANUAL_EXPRESSION = 2,
}

export class GroupByExpression extends PureComponent<GroupByExpressionProps> {
  private ensureExpressionCreated(
    value?: sqlStatement.GroupByExpression
  ): sqlStatement.GroupByExpression {
    if (value !== undefined) {
      return value;
    }
    return new sqlStatement.GroupByExpression();
  }
  private static getVariantIndexByValue(
    value?: sqlStatement.GroupByExpression
  ): GroupByExpressionVariant {
    if (value === undefined || value.hasSelectStatementColumn()) {
      return GroupByExpressionVariant.SELECT_STATEMENT_COLUMN;
    } else if (value.hasTableField()) {
      return GroupByExpressionVariant.TABLE_FIELD;
    } else {
      return GroupByExpressionVariant.MANUAL_EXPRESSION;
    }
  }
  override render() {
    return (
      <div style={{ display: 'flex' }}>
        <VariantField
          components={[
            {
              component: QueryFieldSelect,
              props: {
                value: this.props.value?.selectStatementColumn,
                selectStatementColumns: this.props.selectStatementColumns,
                mode: QueryFieldSelectMode.showNonAggregatedFieldsOnly,
                onChange: (selectStatementColumn: sqlStatement.Column) => {
                  let value = this.ensureExpressionCreated(this.props.value);
                  value.selectStatementColumn = selectStatementColumn;
                  this.props.onChange(value);
                },
              },
              title: 'Query field',
              menuLabel: 'Query field (from SELECT statement)',
            },
            {
              component: FieldSelect,
              props: {
                datasource: this.props.datasource,
                database: this.props.database,
                table: this.props.table,
                forceUpdateState: this.props.forceUpdateState,
                key: this.props.table,
                value: this.props.value?.tableField,
                onChange: (field: DbField) => {
                  let value = this.ensureExpressionCreated(this.props.value);
                  value.tableField = field;
                  this.props.onChange(value);
                },
              },
              title: 'Table field',
              menuLabel: 'Table field',
            },
            {
              component: ExpressionInput,
              props: {
                value: this.props.value?.manualExpression,
                onChange: (expression: string) => {
                  let value = this.ensureExpressionCreated(this.props.value);
                  value.manualExpression = expression;
                  this.props.onChange(value);
                },
              },
              title: 'Manual expression',
              menuLabel: 'Manual expression',
            },
          ]}
          selectedIndex={GroupByExpression.getVariantIndexByValue(this.props.value)}
          onSelectedIndexChanged={(index: number) => {
            let value = this.ensureExpressionCreated(this.props.value);
            switch (index) {
              case GroupByExpressionVariant.SELECT_STATEMENT_COLUMN:
                value.selectStatementColumn = new sqlStatement.Column();
                break;
              case GroupByExpressionVariant.TABLE_FIELD:
                value.tableField = new DbField();
                break;
              case GroupByExpressionVariant.MANUAL_EXPRESSION:
                value.manualExpression = '';
                break;
              default:
                throw new Error('wrong index');
            }
            this.props.onChange(value);
          }}
        />
      </div>
    );
  }
}
