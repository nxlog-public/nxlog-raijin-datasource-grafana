import React, { PureComponent } from 'react';
import { Select } from '@grafana/ui';
import { SelectableValue } from '@grafana/data';

const kinds = ['AVG', 'COUNT', 'MAX', 'MIN', 'SUM', 'FIRST', 'LAST'] as const;
export type AggregationKind = typeof kinds[number];

interface AggregationSelectProps {
  value?: AggregationKind;
  onChange: (value: AggregationKind) => void;
}

export class AggregationSelect extends PureComponent<AggregationSelectProps> {
  override render() {
    return (
      <Select
        value={this.props.value}
        options={kinds.map((aggregation) => ({
          label: aggregation,
          value: aggregation,
        }))}
        placeholder="Choose aggregation"
        onChange={(value: SelectableValue<AggregationKind>) => this.props.onChange(value.value!)}
      />
    );
  }
}
