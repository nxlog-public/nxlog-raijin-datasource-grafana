import React, { PureComponent } from 'react';
import { Select } from '@grafana/ui';
import { getTemplateSrv } from '@grafana/runtime';
import { SelectableValue } from '@grafana/data';

interface GrafanaVariableSelectProps {
  value?: string;
  onChange: (value: string) => void;
}

export class GrafanaVariableSelect extends PureComponent<GrafanaVariableSelectProps> {
  override render() {
    return (
      <Select
        value={this.props.value}
        options={getTemplateSrv()
          .getVariables()
          .map((v) => ({ label: v.name, value: v.name }))}
        noOptionsMessage="No variable found"
        onChange={(value: SelectableValue<string>) => {
          this.props.onChange(value.value!);
        }}
      />
    );
  }
}
