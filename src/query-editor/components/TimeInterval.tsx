import React, { PureComponent } from 'react';
import { Select, Slider } from '@grafana/ui';
import { SelectableValue } from '@grafana/data';
import * as sqlStatement from '../sql-statement';

interface TimeIntervalEditorProps {
  value?: sqlStatement.TimeInterval;
  onChange: (value: sqlStatement.TimeInterval) => void;
}

export class TimeIntervalEditor extends PureComponent<TimeIntervalEditorProps> {
  private ensureTimeInterval(value?: sqlStatement.TimeInterval): sqlStatement.TimeInterval {
    if (value !== undefined) {
      return value;
    }
    return new sqlStatement.TimeInterval();
  }
  override render() {
    return (
      <div style={{ display: 'flex', width: '310px', height: '30px' }}>
        <Slider
          min={1}
          max={999}
          value={this.props.value?.value}
          onChange={(newValue: number) => {
            let timeInterval = this.ensureTimeInterval(this.props.value);
            timeInterval.value = newValue;
            this.props.onChange(timeInterval);
          }}
        />
        <Select<sqlStatement.TimeIntervalPeriod>
          width={15}
          value={{
            label: this.props.value?.timeIntervalPeriod?.toString(),
            value: this.props.value?.timeIntervalPeriod,
          }}
          options={Object.values(sqlStatement.TimeIntervalPeriod).map((key) => ({
            label: key.toString(),
            value: key,
          }))}
          onChange={(value: SelectableValue<sqlStatement.TimeIntervalPeriod>) => {
            let timeInterval = this.ensureTimeInterval(this.props.value);
            timeInterval.timeIntervalPeriod = value.value!;
            this.props.onChange(timeInterval);
          }}
        />
      </div>
    );
  }
}
