import { escapeStringValue, unescapeStringValue } from './ExpressionInput';

describe('Check escapeStringValue/unescapeStringValue', () => {
  function assertEscaping(nonEscaped: string, escaped: string) {
    expect(escapeStringValue(nonEscaped)).toEqual(escaped);
    expect(unescapeStringValue(escaped)).toEqual(nonEscaped);
  }

  it('Should escape and unescape string values', () => {
    assertEscaping("'test'", "'test'");
    assertEscaping("'t'e's't'", "'t\\'e\\'s\\'t'");
    assertEscaping("'", "\\'");
    assertEscaping('"', '\\"');
    assertEscaping("''", "''");
    assertEscaping("'te\\'st'", "'te\\\\\\'st'");
    assertEscaping("'t\0est'", "'t\\0est'");
    assertEscaping("'te\x08st'", "'te\\bst'");
    assertEscaping("'te\x09st'", "'te\\tst'");
    assertEscaping("'te\x1ast'", "'te\\zst'");
    assertEscaping("'te\nst'", "'te\\nst'");
    assertEscaping("'te\rst'", "'te\\rst'");
    assertEscaping("'te%st'", "'te\\%st'");
  });

  it('should unescape undefined', () => {
    expect(unescapeStringValue(undefined)).toEqual('');
  });
});
