import React, { PureComponent } from 'react';
import { SelectableValue } from '@grafana/data';
import { ButtonSelect } from '@grafana/ui';
import { BoolOperation } from '../sql-statement';

interface BooleanOperationSelectProps {
  value?: BoolOperation;
  onChange: (value: BoolOperation) => void;
}

export class BooleanOperationSelect extends PureComponent<BooleanOperationSelectProps> {
  override render() {
    return (
      <ButtonSelect
        value={{ label: this.props.value, value: this.props.value }}
        options={Object.values(BoolOperation).map((key) => ({
          label: key.toString(),
          value: key,
        }))}
        onChange={(item: SelectableValue<BoolOperation>) => {
          this.props.onChange(item.value!);
        }}
      />
    );
  }
}
