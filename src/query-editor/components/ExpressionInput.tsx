import React, { FormEvent, PureComponent } from 'react';
import { Input, Tooltip } from '@grafana/ui';

interface ExpressionInputProps {
  value?: string;
  onChange: (value: string) => void;
}

export const unescapeStringValue = (value?: string): string => {
  if (value === undefined) {
    return '';
  }

  let valueToUnescape = value;
  const isString =
    valueToUnescape.length > 1 && valueToUnescape.startsWith("'") && valueToUnescape.endsWith("'");
  if (isString) {
    valueToUnescape = valueToUnescape.substring(1, valueToUnescape.length - 1);
  }

  const rs = String(valueToUnescape)
    .replace(/\\0/g, '\0')
    .replace(/\\b/g, '\x08')
    .replace(/\\t/g, '\x09')
    .replace(/\\z/g, '\x1a')
    .replace(/\\n/g, '\n')
    .replace(/\\r/g, '\r')
    .replace(/\\"/g, '"')
    .replace(/\\'/g, "'")
    .replace(/\\\\/g, '\\')
    .replace(/\\%/g, '%');

  return isString ? `'${rs}'` : rs;
};

export const escapeStringValue = (value: string): string => {
  // To allow comparisons with string constants we need to allow to enclose them in single
  // quotes. Escaping function does escape single quotes also. So below there is a special
  // treatment for strings
  let valueToEscape = value;
  const isString =
    valueToEscape.length > 1 && valueToEscape.startsWith("'") && valueToEscape.endsWith("'");
  if (isString) {
    valueToEscape = valueToEscape.substring(1, valueToEscape.length - 1);
  }

  const rs = String(valueToEscape).replace(/[\0\x08\x09\x1a\n\r"'\\%]/g, (char) => {
    // taken from here: https://stackoverflow.com/questions/7744912/making-a-javascript-string-sql-friendly
    switch (char) {
      case '\0':
        return '\\0';
      case '\x08':
        return '\\b';
      case '\x09':
        return '\\t';
      case '\x1a':
        return '\\z';
      case '\n':
        return '\\n';
      case '\r':
        return '\\r';
      case '"':
      case "'":
      case '\\':
      case '%':
      default:
        return '\\' + char;
    }
  });

  return isString ? `'${rs}'` : rs;
};

export class ExpressionInput extends PureComponent<ExpressionInputProps> {
  override render() {
    return (
      <Tooltip
        content={
          'To enter a string constant - enclose it into single quotes (pay attention, single quotes in all other cases are escaped)'
        }
      >
        <Input
          width={25}
          value={unescapeStringValue(this.props.value)}
          onChange={(value: FormEvent<HTMLInputElement>) => {
            this.props.onChange(escapeStringValue(value.currentTarget.value.trim()));
          }}
          placeholder="Enter arbitrary expression"
        />
      </Tooltip>
    );
  }
}
