import React, { PureComponent } from 'react';
import { BooleanOperationSelect } from './BooleanOperationSelect';
import { VariantField } from './Variant';
import { FieldSelect } from './FieldSelect';
import { ExpressionInput } from './ExpressionInput';
import { CommonProps } from './types';
import { GrafanaVariableSelect } from './GrafanaVariableSelect';
import { DbField } from '../../DatabaseClient';
import * as sqlStatement from '../sql-statement';
import { GrafanaVariable } from '../sql-statement/WhereClause';

interface WhereExpressionArgumentProps extends CommonProps {
  value?: sqlStatement.WhereExpressionArgument;
  onChange: (value: sqlStatement.WhereExpressionArgument) => void;
}

enum WhereExpressionArgumentVariant {
  FIELD = 0,
  MANUAL_EXPRESSION = 1,
  GRAFANA_VARIABLE = 2,
}

class WhereExpressionArgument extends PureComponent<WhereExpressionArgumentProps> {
  private ensureExpressionCreated(
    value?: sqlStatement.WhereExpressionArgument
  ): sqlStatement.WhereExpressionArgument {
    if (value !== undefined) {
      return value;
    }
    return new sqlStatement.WhereExpressionArgument();
  }

  private getVariantIndexByValue(
    value?: sqlStatement.WhereExpressionArgument
  ): WhereExpressionArgumentVariant {
    if (value === undefined || value.hasDbField()) {
      return WhereExpressionArgumentVariant.FIELD;
    } else if (value.hasManualExpression()) {
      return WhereExpressionArgumentVariant.MANUAL_EXPRESSION;
    }
    return WhereExpressionArgumentVariant.GRAFANA_VARIABLE;
  }

  override render() {
    return (
      <VariantField
        components={[
          {
            // WhereExpressionArgumentVariant.FIELD
            component: FieldSelect,
            props: {
              datasource: this.props.datasource,
              database: this.props.database,
              table: this.props.table,
              forceUpdateState: this.props.forceUpdateState,
              key: this.props.table,
              value: this.props.value?.dbField,
              onChange: (field: DbField) => {
                let value = this.ensureExpressionCreated(this.props.value);
                value.dbField = field;
                this.props.onChange(value);
              },
            },
            title: 'Table field',
            menuLabel: 'Table field',
          },
          {
            // WhereExpressionArgumentVariant.MANUAL_EXPRESSION
            component: ExpressionInput,
            props: {
              value: this.props.value?.manualExpression,
              onChange: (expression: string) => {
                let value = this.ensureExpressionCreated(this.props.value);
                value.manualExpression = expression;
                this.props.onChange(value);
              },
            },
            title: 'Manual expression',
            menuLabel: 'Manual expression',
          },
          {
            // WhereExpressionArgumentVariant.GRAFANA_VARIABLE
            component: GrafanaVariableSelect,
            props: {
              value: this.props.value?.grafanaVariable?.name,
              onChange: (grafanaVariableName: string) => {
                let value = this.ensureExpressionCreated(this.props.value);
                value.grafanaVariable = new GrafanaVariable(grafanaVariableName);
                this.props.onChange(value);
              },
            },
            title: 'Grafana variable',
            menuLabel: 'Grafana variable',
          },
        ]}
        selectedIndex={this.getVariantIndexByValue(this.props.value)}
        onSelectedIndexChanged={(index: number) => {
          let value = this.ensureExpressionCreated(this.props.value);
          switch (index) {
            case WhereExpressionArgumentVariant.FIELD:
              value.dbField = new DbField();
              break;
            case WhereExpressionArgumentVariant.MANUAL_EXPRESSION:
              value.manualExpression = '';
              break;
            case WhereExpressionArgumentVariant.GRAFANA_VARIABLE:
              value.grafanaVariable = new GrafanaVariable();
              break;
            default:
              throw new Error('wrong index');
          }
          this.props.onChange(value);
        }}
      />
    );
  }
}

export interface WhereExpressionProps extends CommonProps {
  value: sqlStatement.WhereExpression;
  onChange: (value: sqlStatement.WhereExpression) => void;
}

export class WhereExpression extends PureComponent<WhereExpressionProps> {
  override render() {
    return (
      <div style={{ display: 'flex' }}>
        <div>
          <WhereExpressionArgument
            datasource={this.props.datasource}
            database={this.props.database}
            table={this.props.table}
            forceUpdateState={this.props.forceUpdateState}
            value={this.props.value.leftExpression}
            onChange={(whereExpression: sqlStatement.WhereExpressionArgument) => {
              let { value } = this.props;
              value.leftExpression = whereExpression;
              this.props.onChange(value);
            }}
          />
        </div>
        <div style={{ marginTop: 30 }}>
          <BooleanOperationSelect
            value={this.props.value.operation}
            onChange={(operation: sqlStatement.BoolOperation) => {
              let { value } = this.props;
              value.operation = operation;
              this.props.onChange(value);
            }}
          />
        </div>
        <div>
          <WhereExpressionArgument
            datasource={this.props.datasource}
            database={this.props.database}
            table={this.props.table}
            forceUpdateState={this.props.forceUpdateState}
            value={this.props.value.rightExpression}
            onChange={(whereExpression: sqlStatement.WhereExpressionArgument) => {
              let { value } = this.props;
              value.rightExpression = whereExpression;
              this.props.onChange(value);
            }}
          />
        </div>
      </div>
    );
  }
}
