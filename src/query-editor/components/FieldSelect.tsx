import React, { PureComponent } from 'react';
import { SelectableValue } from '@grafana/data';
import { AsyncSelect } from '@grafana/ui';
import { CommonProps } from './types';
import { DatabaseClient, DbField } from '../../DatabaseClient';

interface FieldSelectProps extends CommonProps {
  value?: DbField;
  allowAsterisk?: boolean;
  onChange: (value: DbField) => void;
  filterValue?: (value: DbField) => boolean;
}

export class FieldSelect extends PureComponent<FieldSelectProps> {
  LoadFields = async (): Promise<Array<SelectableValue<DbField>>> => {
    const databaseClient = new DatabaseClient(this.props.datasource);
    let fields: DbField[] = await databaseClient.getFieldsList(
      this.props.database,
      this.props.table
    );
    if (this.props.allowAsterisk) {
      const asteriskField = DbField.createAsteriskField(this.props.database, this.props.table);
      fields.unshift(asteriskField);
    }
    return fields
      .filter((field) => this.props.filterValue === undefined || this.props.filterValue(field))
      .map((field) => ({ label: field.name, value: field }));
  };

  override render() {
    return (
      <div>
        <AsyncSelect<DbField>
          key={`${this.props.forceUpdateState}:${this.props.table}`}
          width={25}
          loadOptions={this.LoadFields}
          defaultOptions={true}
          isSearchable={false}
          cacheOptions={true}
          value={{ label: this.props.value?.name, value: this.props.value }}
          onChange={(value: SelectableValue<DbField>) => {
            const field: DbField = value.value!;
            this.props.onChange(field);
          }}
          noOptionsMessage={this.props.table ? 'No fields found...' : 'Choose the table first...'}
        />
      </div>
    );
  }
}
