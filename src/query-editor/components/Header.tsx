import React, { PureComponent } from 'react';
import { GrafanaTheme2 } from '@grafana/data';
import { css } from '@emotion/css';
import { config } from '@grafana/runtime';
import { Button, Icon, IconName } from '@grafana/ui';

const getStyles = (theme: GrafanaTheme2) => {
  return {
    header: css({
      display: 'flex',
      alignItems: 'left',
      paddingTop: '5px',
      color: theme.colors.text.secondary,
      borderBottom: `1px solid ${theme.colors.border.weak}`,
      '&:hover .show-on-hover': css({
        opacity: 1,
      }),
    }),
    headerHiddenButtons: css({
      opacity: 0.4,
      transition: theme.transitions.create(['opacity'], {
        duration: theme.transitions.duration.short,
      }),
    }),
    grow: css({
      flexGrow: 1,
    }),
    dropdown: css({
      position: 'relative',
      display: 'inline-block',
    }),
    dropdownContentHidden: css({
      display: 'none',
      position: 'absolute',
      backgroundColor: theme.colors.background.primary,
      minWidth: '160px',
      boxShadow: `${theme.shadows.z3}`,
      zIndex: theme.zIndex.dropdown,
    }),
    dropdownContent: css({
      display: 'block',
      position: 'absolute',
      backgroundColor: theme.colors.background.primary,
      minWidth: '160px',
      boxShadow: `${theme.shadows.z3}`,
      zIndex: theme.zIndex.dropdown,
    }),
    dropdownElement: css({
      color: `${theme.colors.text.primary}`,
      padding: theme.spacing('5px', '12px', '5px', '10px'),
      cursor: 'pointer',
      textDecoration: 'none',
      display: 'block',
      ':hover': {
        backgroundColor: `${theme.colors.action.hover}`,
        color: `${theme.colors.text.primary}`,
      },
    }),
  };
};

interface BaseButtonProps {
  icon: IconName;
  active?: boolean;
  hint?: string;
}

interface HeaderButtonProps extends BaseButtonProps {
  onClick: () => void;
}

export class HeaderButton extends PureComponent<HeaderButtonProps> {
  private onClick = this.props.onClick.bind(this);
  override render() {
    return (
      <Button
        icon={this.props.icon}
        size="sm"
        fill={this.props.active ? 'solid' : 'text'}
        variant="secondary"
        title={this.props.hint}
        onClick={this.onClick}
      />
    );
  }
}

interface MenuItemProps {
  label: string;
  checked: boolean;
  onClick: () => void;
}

class MenuItem extends PureComponent<MenuItemProps> {
  private onClick = this.props.onClick.bind(this);
  override render() {
    const style = getStyles(config.theme2);
    return (
      <div className={`${style.dropdownElement}`} onClick={this.onClick}>
        {this.props.checked && <Icon name={'check'} />}
        {this.props.label}
      </div>
    );
  }
}

interface HeaderButtonMenuOption {
  label: string;
  onClick: () => void;
}

interface HeaderButtonMenuProps {
  button: BaseButtonProps;
  items: HeaderButtonMenuOption[];
  markAactiveOption?: boolean;
  checkedOption?: number;
  onMenuOpen?: () => void;
  onMenuClose?: () => void;
}

interface HeaderButtonMenuState {
  isOpen: boolean;
}

class HeaderButtonMenu extends PureComponent<HeaderButtonMenuProps, HeaderButtonMenuState> {
  private contentRef = React.createRef<HTMLDivElement>();
  private buttonRef = React.createRef<HTMLDivElement>();

  constructor(props: any) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  override componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  override componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  toggleMenu(isOpened: boolean) {
    this.setState({
      isOpen: isOpened,
    });
    if (isOpened && this.props.onMenuOpen) {
      this.props.onMenuOpen();
    }
    if (!isOpened && this.props.onMenuClose) {
      this.props.onMenuClose();
    }
  }

  handleClickOutside(event: MouseEvent) {
    if (this.buttonRef && this.buttonRef.current?.contains(event.target as Node)) {
      return;
    }
    if (this.contentRef && !this.contentRef.current?.contains(event.target as Node)) {
      this.toggleMenu(false);
    }
  }

  override render() {
    const style = getStyles(config.theme2);
    return (
      <div ref={this.buttonRef} className={`${style.dropdown}`}>
        <HeaderButton
          {...this.props.button}
          active={this.state.isOpen}
          onClick={() => {
            this.toggleMenu(!this.state.isOpen);
          }}
        />
        <div
          ref={this.contentRef}
          className={
            this.state.isOpen ? `${style.dropdownContent}` : `${style.dropdownContentHidden}`
          }
        >
          {this.props.items.map((item, index) => (
            <MenuItem
              key={index}
              label={item.label}
              checked={this.props.checkedOption !== undefined && this.props.checkedOption === index}
              onClick={() => {
                item.onClick();
                this.toggleMenu(false);
              }}
            />
          ))}
        </div>
      </div>
    );
  }
}

export type HeaderButtonList = Array<HeaderButtonProps | HeaderButtonMenuProps>;

export interface HeaderProps {
  title: string;
  buttons?: HeaderButtonList;
}

interface HeaderState {
  isButtonSubMenuOpen: boolean; // just to apply a style if one of menus is open
}

export class Header extends PureComponent<HeaderProps, HeaderState> {
  constructor(props: any) {
    super(props);
    this.state = { isButtonSubMenuOpen: false };
  }
  override render() {
    const styles = getStyles(config.theme2);
    return (
      <div className={`${styles.header}`}>
        <div>[{this.props.title}]</div>
        <div className={styles.grow} />
        <div
          className={
            !this.state.isButtonSubMenuOpen ? `${styles.headerHiddenButtons} show-on-hover` : ''
          }
        >
          {this.props.buttons?.map((button, index) => {
            if ('items' in button) {
              return (
                <HeaderButtonMenu
                  key={index}
                  {...(button as HeaderButtonMenuProps)}
                  onMenuOpen={() => {
                    this.setState({ isButtonSubMenuOpen: true });
                  }}
                  onMenuClose={() => {
                    this.setState({ isButtonSubMenuOpen: false });
                  }}
                />
              );
            } else {
              return <HeaderButton key={index} {...(button as HeaderButtonProps)} />;
            }
          })}
        </div>
      </div>
    );
  }
}

function getWithHeaderStyles() {
  return {
    grid: css({
      display: 'grid',
      gridTemplateColumns: '1fr',
      gridTemplateRows: '2fr',
    }),
    field: css({
      display: 'flex',
    }),
    grow: css({
      flexGrow: 1,
    }),
  };
}

interface WithHeaderProps extends HeaderProps {
  children: JSX.Element;
}

export function WithHeader(props: WithHeaderProps): any {
  const styles = getWithHeaderStyles();
  return (
    <div className={`${styles.grid}`}>
      <Header title={props.title} buttons={props.buttons} />
      <div className={`${styles.field}`}>{props.children}</div>
    </div>
  );
}
