import { DataSource } from '../../datasource';

export interface CommonProps {
  datasource: DataSource;
  database: string;
  table: string;
  forceUpdateState: number;
}
