import React, { PureComponent } from 'react';
import { SelectableValue } from '@grafana/data';
import { AsyncSelect, Field } from '@grafana/ui';
import { DataSource } from '../../datasource';
import { DatabaseClient } from '../../DatabaseClient';

interface DatabaseSelectProps {
  datasource: DataSource;
  value?: string;
  onChange: (value: string) => void;
}

export class DatabaseSelect extends PureComponent<DatabaseSelectProps> {
  private LoadDatabases = async (): Promise<Array<SelectableValue<string>>> => {
    const databaseClient = new DatabaseClient(this.props.datasource);
    const databases: string[] = await databaseClient.getDatabasesList();
    return databases.map((db) => ({ label: db, value: db }));
  };

  override render() {
    return (
      <div>
        <Field label="Select database">
          <AsyncSelect
            width={25}
            value={{ label: this.props.value, value: this.props.value }}
            loadOptions={this.LoadDatabases}
            defaultOptions={true}
            isSearchable={false}
            onChange={(value: SelectableValue<string>) => this.props.onChange(value.value || '')}
          />
        </Field>
      </div>
    );
  }
}
