import React, { PureComponent } from 'react';
import { DataSource } from '../../datasource';
import { SelectableValue } from '@grafana/data';
import { AsyncSelect, Field } from '@grafana/ui';
import { DatabaseClient } from '../../DatabaseClient';

interface TableSelectProps {
  datasource: DataSource;
  database: string;
  value?: string;
  onChange: (value: string) => void;
}

export class TableSelect extends PureComponent<TableSelectProps> {
  private LoadTables = async (): Promise<Array<SelectableValue<string>>> => {
    const databaseClient = new DatabaseClient(this.props.datasource);
    const tables: string[] = await databaseClient.getTablesList(this.props.database);
    return tables.map((table) => ({ label: table, value: table }));
  };

  override render() {
    return (
      <div>
        <Field label="Select table">
          <AsyncSelect
            key={this.props.database}
            value={{ label: this.props.value, value: this.props.value }}
            width={25}
            loadOptions={this.LoadTables}
            defaultOptions={true}
            isSearchable={false}
            onChange={(value: SelectableValue<string>) => this.props.onChange(value.value || '')}
          />
        </Field>
      </div>
    );
  }
}
