import React, { FormEvent, PureComponent } from 'react';
import { css } from '@emotion/css';
import {
  Button,
  Checkbox,
  Field,
  Label,
  TextArea,
  Tooltip,
  ConfirmModal,
  Alert,
} from '@grafana/ui';
import { DataSource } from '../datasource';
import { CommonProps } from './components/types';
import { config } from '@grafana/runtime';
import { DbField } from '../DatabaseClient';
import * as ui from './components';
import * as sqlStatement from './sql-statement';
import { QueryBuilderRow } from './QueryBuilderRow';
import { GetLastQueryError } from '../common';

interface QueryBuilderProps {
  datasource: DataSource;
  selectStatement: sqlStatement.SelectStatement;
  onChange: () => void;
  onQueryRun: () => void;
}

interface QueryBuilderState {
  forceUpdateDatabase: number;
  confirmDatabaseOrTableChange: boolean;
  newDatabaseName: string;
  newTableName: string;
  showRawSql: boolean;
  runQuerySynchronously: boolean;
}

export class QueryBuilder extends PureComponent<QueryBuilderProps, QueryBuilderState> {
  constructor(props: QueryBuilderProps) {
    super(props);
    this.state = {
      forceUpdateDatabase: 0,
      confirmDatabaseOrTableChange: false,
      newDatabaseName: '',
      newTableName: '',
      showRawSql: true,
      runQuerySynchronously: false,
    };
  }

  private onChange() {
    this.props.onChange();
    if (this.state.runQuerySynchronously) {
      this.props.onQueryRun();
    }
  }

  getCommonProps(): CommonProps {
    return {
      database: this.props.selectStatement.fromClause.databaseName,
      table: this.props.selectStatement.fromClause.tableName,
      datasource: this.props.datasource,
      forceUpdateState: this.state.forceUpdateDatabase,
    };
  }

  rawSqlJsx = () => {
    return (
      <div style={{ display: 'flex', height: '100%', width: '100%' }}>
        <QueryBuilderRow height={'100%'}>
          <div
            style={{
              display: 'grid',
              gridTemplateColumns: '1fr',
              gridTemplateRows: 'auto 1fr',
            }}
          >
            <Field label={'Raw query'}>
              <div />
            </Field>
            <TextArea
              readOnly={true}
              style={{ height: '100%' }}
              value={this.props.selectStatement.getSql()}
            />
          </div>
        </QueryBuilderRow>
      </div>
    );
  };

  commonSettingsJsx = () => {
    const styles = {
      main: css({
        display: 'flex',
        gap: '16px',
        padding: config.theme2.spacing(0.5, 0.5, 0.5, 0),
      }),
      subArea: css({
        display: 'flex',
        gap: '3px',
      }),
    };
    const OnDatabaseOrTableChange = (databaseName: string, tableName: string) => {
      this.props.selectStatement.reset();
      this.props.selectStatement.fromClause.databaseName = databaseName;
      this.props.selectStatement.fromClause.tableName = tableName;
      this.onChange();
    };
    return (
      <QueryBuilderRow>
        <div className={`${styles.main}`} key={this.state.forceUpdateDatabase}>
          <div className={`${styles.subArea}`}>
            <ConfirmModal
              isOpen={this.state.confirmDatabaseOrTableChange}
              title={'Database/table was changed'}
              confirmText={'Ok'}
              dismissText={'Cancel'}
              onConfirm={() => {
                OnDatabaseOrTableChange(this.state.newDatabaseName, this.state.newTableName);
                this.setState({ ...this.state, confirmDatabaseOrTableChange: false });
              }}
              onDismiss={() => {
                this.setState({ ...this.state, confirmDatabaseOrTableChange: false });
              }}
              body={<div>All configured fields/expression/etc. will be removed. Continue?</div>}
            />
            <ui.DatabaseSelect
              key={this.state.forceUpdateDatabase}
              datasource={this.props.datasource}
              value={this.props.selectStatement.fromClause.databaseName}
              onChange={(databaseName: string) => {
                if (this.props.selectStatement.hasChildren()) {
                  this.setState({
                    ...this.state,
                    confirmDatabaseOrTableChange: true,
                    newDatabaseName: databaseName,
                    newTableName: '',
                  });
                } else {
                  OnDatabaseOrTableChange(databaseName, '');
                }
              }}
            />
            <ui.TableSelect
              key={`${this.state.forceUpdateDatabase}.${this.props.selectStatement.fromClause.databaseName}`}
              datasource={this.props.datasource}
              database={this.props.selectStatement.fromClause.databaseName}
              value={this.props.selectStatement.fromClause.tableName}
              onChange={(tableName: string) => {
                const { selectStatement } = this.props;
                if (selectStatement.hasChildren()) {
                  this.setState({
                    ...this.state,
                    confirmDatabaseOrTableChange: true,
                    newDatabaseName: selectStatement.fromClause.databaseName,
                    newTableName: tableName,
                  });
                } else {
                  OnDatabaseOrTableChange(selectStatement.fromClause.databaseName, tableName);
                }
              }}
            />
            <Tooltip content={'Update database structure'}>
              <Button
                variant={'secondary'}
                icon={'sync'}
                style={{ marginTop: 19 }}
                onClick={() => {
                  this.setState({
                    ...this.state,
                    forceUpdateDatabase: this.state.forceUpdateDatabase + 1,
                  });
                }}
              />
            </Tooltip>
          </div>
          <Checkbox
            value={this.state.showRawSql}
            label={'Raw Sql'}
            description={'show raw Sql area'}
            onChange={(event: FormEvent<HTMLInputElement>) => {
              this.setState({
                ...this.state,
                showRawSql: event.currentTarget.checked,
              });
            }}
          />
          <Checkbox
            value={this.state.runQuerySynchronously}
            label={'On the fly mode'}
            description={'Execute query on every change'}
            onChange={(event: FormEvent<HTMLInputElement>) => {
              this.setState({
                ...this.state,
                runQuerySynchronously: event.currentTarget.checked,
              });
            }}
          />
          <Checkbox
            value={this.props.selectStatement.syncronizeColumns}
            label={'Synchronize columns'}
            description={'Synchronize Group By/Having clauses with column list'}
            onChange={(event: FormEvent<HTMLInputElement>) => {
              this.props.selectStatement.syncronizeColumns = event.currentTarget.checked;
              this.onChange();
            }}
          />
        </div>
      </QueryBuilderRow>
    );
  };

  timeColumnJsx = () => {
    return (
      <QueryBuilderRow>
        <Label>Time column settings</Label>
        <div style={{ display: 'flex', gap: '10px' }}>
          <Checkbox
            value={this.props.selectStatement.timeColumn.enabled}
            label={'Time-series data'}
            description={'Check to select a time column'}
            onChange={(event: FormEvent<HTMLInputElement>) => {
              this.props.selectStatement.timeColumn.enabled = event.currentTarget.checked;
              this.onChange();
            }}
          />
          {this.props.selectStatement.timeColumn.enabled && (
            <div style={{ display: 'flex', gap: '10px' }}>
              {!this.props.selectStatement.timeColumn.autogenerated && (
                <div>
                  <Label>Time column:</Label>
                  <ui.FieldSelect
                    key={this.state.forceUpdateDatabase}
                    {...this.getCommonProps()}
                    value={this.props.selectStatement.timeColumn.field}
                    filterValue={(field: DbField) => {
                      return (
                        field.type === 'DATETIME' ||
                        field.type === 'DATE' ||
                        field.type === 'TIMESTAMP' ||
                        field.type === 'TIME'
                      );
                    }}
                    onChange={(timeField: DbField) => {
                      this.props.selectStatement.timeColumn.field = timeField;
                      this.onChange();
                    }}
                  />
                </div>
              )}
              {!this.props.selectStatement.timeColumn.autogenerated && (
                <div>
                  <Checkbox
                    value={this.props.selectStatement.timeColumn.groupByTimeInterval}
                    label={'group by time interval'}
                    onChange={(event: FormEvent<HTMLInputElement>) => {
                      this.props.selectStatement.timeColumn.groupByTimeInterval =
                        event.currentTarget.checked;
                      this.onChange();
                    }}
                  />
                  {this.props.selectStatement.timeColumn.groupByTimeInterval && (
                    <ui.TimeIntervalEditor
                      value={this.props.selectStatement.timeColumn.timeInterval}
                      onChange={(timeInterval: sqlStatement.TimeInterval) => {
                        this.props.selectStatement.timeColumn.timeInterval = timeInterval;
                        this.onChange();
                      }}
                    />
                  )}
                </div>
              )}
              <div style={{ maxWidth: '300px' }}>
                <Checkbox
                  value={this.props.selectStatement.timeColumn.autogenerated}
                  label={'autogenerate'}
                  description={
                    'Auto fill the time column to display non-timeseries data like as they were the time-series'
                  }
                  onChange={(event: FormEvent<HTMLInputElement>) => {
                    this.props.selectStatement.timeColumn.autogenerated =
                      event.currentTarget.checked;
                    if (!this.props.selectStatement.timeColumn.autogenerated) {
                      this.props.selectStatement.timeColumn.groupByTimeInterval = false;
                    }
                    this.onChange();
                  }}
                />
              </div>
            </div>
          )}
        </div>
      </QueryBuilderRow>
    );
  };

  selectFieldsJsx = () => {
    return (
      <QueryBuilderRow>
        <Label>SELECT:</Label>
        <ui.ComponentsArray<
          ui.SelectStatementColumnProps,
          ui.SelectStatementColumnState,
          ui.SelectStatementColumn
        >
          key={this.props.selectStatement.selectColumns.childrenCount()}
          addButton={{ text: 'Add column', tooltip: 'Add new column to the query' }}
          itemType={ui.SelectStatementColumn}
          itemsProps={this.props.selectStatement.selectColumns.map((column, index) => ({
            ...this.getCommonProps(),
            value: column,
            onChange: (changedColumn: sqlStatement.Column) => {
              this.props.selectStatement.selectColumns.update(index, changedColumn);
              this.onChange();
            },
          }))}
          onAddButtonClicked={() => {
            this.props.selectStatement.selectColumns.add(new sqlStatement.Column());
            this.onChange();
          }}
          onRemoveButtonClicked={(index: number) => {
            this.props.selectStatement.selectColumns.remove(index);
            this.onChange();
          }}
        />
      </QueryBuilderRow>
    );
  };

  whereConditionsJsx = () => {
    return (
      <QueryBuilderRow>
        <Label>WHERE:</Label>
        <ui.ComponentsArray<ui.WhereExpressionProps, {}, ui.WhereExpression>
          key={this.props.selectStatement.whereClause.childrenCount()}
          addButton={{
            text: 'Add WHERE expression',
            tooltip: 'Add new WHERE expression to the query',
          }}
          itemType={ui.WhereExpression}
          itemsProps={this.props.selectStatement.whereClause.map((whereExpression, index) => ({
            ...this.getCommonProps(),
            value: whereExpression,
            onChange: (changedWhereExpression: sqlStatement.WhereExpression) => {
              this.props.selectStatement.whereClause.update(index, changedWhereExpression);
              this.onChange();
            },
          }))}
          onAddButtonClicked={() => {
            this.props.selectStatement.whereClause.add(new sqlStatement.WhereExpression());
            this.onChange();
          }}
          onRemoveButtonClicked={(index: number) => {
            this.props.selectStatement.whereClause.remove(index);
            this.onChange();
          }}
        />
      </QueryBuilderRow>
    );
  };

  groupByFieldsJsx = () => {
    return (
      <QueryBuilderRow>
        <Label>GROUP BY:</Label>
        <div style={{ display: 'grid' }}>
          <ui.ComponentsArray<ui.GroupByExpressionProps, {}, ui.GroupByExpression>
            key={this.props.selectStatement.groupByClause.childrenCount()}
            itemType={ui.GroupByExpression}
            itemsProps={this.props.selectStatement.groupByClause.map(
              (groupByExpression, index) => ({
                ...this.getCommonProps(),
                value: groupByExpression,
                selectStatementColumns: this.props.selectStatement.selectColumns,
                onChange: (changedGroupByExpression: sqlStatement.GroupByExpression) => {
                  this.props.selectStatement.groupByClause.update(index, changedGroupByExpression);
                  this.onChange();
                },
              })
            )}
            addButton={{
              text: 'Add Group By expression',
              tooltip: 'Add new GROUP BY expression to the query',
            }}
            onAddButtonClicked={() => {
              this.props.selectStatement.groupByClause.add(new sqlStatement.GroupByExpression());
              this.onChange();
            }}
            onRemoveButtonClicked={(index: number) => {
              this.props.selectStatement.groupByClause.remove(index);
              this.onChange();
            }}
          />
        </div>
      </QueryBuilderRow>
    );
  };

  havingFieldsJsx = () => {
    return (
      <QueryBuilderRow>
        <Label>HAVING:</Label>
        <ui.ComponentsArray<ui.HavingExpressionProps, {}, ui.HavingExpression>
          key={this.props.selectStatement.havingClause.childrenCount()}
          itemType={ui.HavingExpression}
          itemsProps={this.props.selectStatement.havingClause.map((havingExpression, index) => ({
            ...this.getCommonProps(),
            value: havingExpression,
            selectStatementColumns: this.props.selectStatement.selectColumns,
            onChange: (changedHavingExpression: sqlStatement.HavingExpression) => {
              this.props.selectStatement.havingClause.update(index, changedHavingExpression);
              this.onChange();
            },
          }))}
          addButton={{
            text: 'Add Having expression',
            tooltip: 'Add new HAVING expression to the query',
          }}
          onAddButtonClicked={() => {
            this.props.selectStatement.havingClause.add(new sqlStatement.HavingExpression());
            this.onChange();
          }}
          onRemoveButtonClicked={(index: number) => {
            this.props.selectStatement.havingClause.remove(index);
            this.onChange();
          }}
        />
      </QueryBuilderRow>
    );
  };

  override render() {
    return (
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: this.state.showRawSql ? '3fr 1fr' : '1fr',
          gridTemplateRows: '1fr',
          columnGap: '5px',
          width: '100%',
        }}
      >
        <div style={{ gridColumn: '1' }}>
          <div
            style={{
              display: 'grid',
              gridTemplateColumns: '1fr',
              rowGap: '5px',
            }}
          >
            {GetLastQueryError() !== '' && (
              <QueryBuilderRow>
                <Alert title={GetLastQueryError()} severity="error" />
              </QueryBuilderRow>
            )}
            <this.commonSettingsJsx />
            <this.timeColumnJsx />
            <this.selectFieldsJsx />
            <this.whereConditionsJsx />
            <this.groupByFieldsJsx />
            <this.havingFieldsJsx />
            <div style={{ display: 'flex' }}>
              <Button onClick={() => this.props.onQueryRun()}>Run query</Button>
            </div>
          </div>
        </div>
        {this.state.showRawSql && (
          <div style={{ gridColumn: '2', gridRow: '1/8', minHeight: '200px', width: '100%' }}>
            <this.rawSqlJsx />
          </div>
        )}
      </div>
    );
  }
}
