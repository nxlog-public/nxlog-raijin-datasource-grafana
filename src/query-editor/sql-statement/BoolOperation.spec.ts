import { BoolOperation, FormatBoolOperationArgument } from './BoolOperation';

describe('FormatBoolOperationArgument', () => {
  it('should return same argument for basic operations', () => {
    const testArgument = 'foo';
    expect(FormatBoolOperationArgument(BoolOperation.Equal, testArgument)).toEqual(testArgument);
    expect(FormatBoolOperationArgument(BoolOperation.LessThan, testArgument)).toEqual(testArgument);
    expect(FormatBoolOperationArgument(BoolOperation.GreaterThan, testArgument)).toEqual(
      testArgument
    );
    expect(FormatBoolOperationArgument(BoolOperation.LessOrEqual, testArgument)).toEqual(
      testArgument
    );
    expect(FormatBoolOperationArgument(BoolOperation.GreaterOrEqual, testArgument)).toEqual(
      testArgument
    );
    expect(FormatBoolOperationArgument(BoolOperation.NotEqual, testArgument)).toEqual(testArgument);
  });

  it('should add parentheses for IN/NOT IN', () => {
    const testArgument = 'foo';
    expect(FormatBoolOperationArgument(BoolOperation.In, testArgument)).toEqual(
      `(${testArgument})`
    );
    expect(FormatBoolOperationArgument(BoolOperation.NotIn, testArgument)).toEqual(
      `(${testArgument})`
    );
  });

  it('should process unknown/undefined inputs', () => {
    expect(FormatBoolOperationArgument('UNKNOWN' as unknown as BoolOperation, 'a')).toEqual('a');
    expect(FormatBoolOperationArgument(BoolOperation.Equal, undefined)).toEqual('');
    expect(FormatBoolOperationArgument(BoolOperation.In, undefined)).toEqual('()');
  });
});
