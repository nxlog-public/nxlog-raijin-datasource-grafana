import {
  HavingClause,
  HavingExpression,
  HavingExpressionArgument,
  HavingExpressionArgumentDetails,
  HavingExpressionDetails,
} from './HavingClause';
import { Column } from './Column';
import { DbField } from '../../DatabaseClient';
import { BoolOperation } from './BoolOperation';
import { isString } from 'lodash';

describe('HavingExpressionArgument', () => {
  it('should represent a column by default', () => {
    const havingExpressionArgument = new HavingExpressionArgument();
    expect(havingExpressionArgument.hasSelectStatementColumn()).toEqual(true);
  });

  it('should compare to another HavingExpressionArgument', () => {
    const column1 = new Column('SUM', 'foo', undefined, 'a + b');
    const column2 = new Column('AVG', 'foo', undefined, 'a + b');

    const manualExpression1 = 'a + b';
    const manualExpression2 = 'a + c';

    expect(
      new HavingExpressionArgument(column1).equalsTo(new HavingExpressionArgument(column1))
    ).toEqual(true);
    expect(
      new HavingExpressionArgument(column1).equalsTo(new HavingExpressionArgument(column2))
    ).toEqual(false);
    expect(
      new HavingExpressionArgument(column1).equalsTo(
        new HavingExpressionArgument(manualExpression1)
      )
    ).toEqual(false);

    expect(
      new HavingExpressionArgument(manualExpression1).equalsTo(
        new HavingExpressionArgument(manualExpression1)
      )
    ).toEqual(true);
    expect(
      new HavingExpressionArgument(manualExpression1).equalsTo(
        new HavingExpressionArgument(manualExpression2)
      )
    ).toEqual(false);
    expect(
      new HavingExpressionArgument(manualExpression1).equalsTo(
        new HavingExpressionArgument(column1)
      )
    ).toEqual(false);
  });

  it('should deserialize', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const column1 = new Column('SUM', 'foo', dbField);
    const column2 = new Column('SUM', 'foo', undefined, 'a + b');
    const manualExpression = 'a + b';

    const checkDeserialization = (field?: Column | string): void => {
      const havingExpressionArgument = new HavingExpressionArgument(field);
      const serializedHavingExpressionArgument = havingExpressionArgument.serialize();
      const expectedSerializedHavingExpressionArgument: HavingExpressionArgumentDetails = {
        selectStatementColumn: field instanceof Column ? (field as Column)?.serialize() : undefined,
        manualExpression: isString(field) ? (field as string) || '' : '',
      };

      expect(serializedHavingExpressionArgument).toEqual(
        expectedSerializedHavingExpressionArgument
      );
      const deserializedHavingExpressionArgument = HavingExpressionArgument.deserialize(
        serializedHavingExpressionArgument
      );
      expect(deserializedHavingExpressionArgument.equalsTo(havingExpressionArgument)).toEqual(true);
    };

    checkDeserialization(column1);
    checkDeserialization(column2);
    checkDeserialization(manualExpression);
  });

  it('should return proper sql', () => {
    const column = new Column('SUM', 'foo', undefined, 'a + b');

    // the HavingExpressionArgument can be represented by: a Column, a manual expression;
    // being represented by a Column, HavingExpressionArgument.getSql() should return
    // Column.getSqlWithoutAlias()
    expect(new HavingExpressionArgument(column).getSql()).toEqual(column.getSqlWithoutAlias());
    // being represented by a manual expression, HavingExpressionArgument.getSql() should return
    // that manual expression
    expect(new HavingExpressionArgument('a + b').getSql()).toEqual('a + b');
  });

  it('should clear other fields on setting one', () => {
    const column = new Column('SUM', 'foo', undefined, 'a + b');
    const havingExpressionArgument = new HavingExpressionArgument(column);
    expect(havingExpressionArgument.hasSelectStatementColumn()).toEqual(true);
    expect(havingExpressionArgument.manualExpression).toEqual('');

    havingExpressionArgument.manualExpression = 'a + b';
    expect(havingExpressionArgument.hasSelectStatementColumn()).toEqual(false);
    expect(havingExpressionArgument.manualExpression).not.toEqual('');

    havingExpressionArgument.selectStatementColumn = column;
    expect(havingExpressionArgument.hasSelectStatementColumn()).toEqual(true);
    expect(havingExpressionArgument.manualExpression).toEqual('');
  });
});

describe('HavingExpression', () => {
  const column1 = new Column('SUM', 'foo', undefined, 'a + b');
  const column2 = new Column('AVG', 'foo', undefined, 'a + b');

  const manualExpression1 = 'a + b';
  const manualExpression2 = 'a + c';

  const argumentLeft1 = new HavingExpressionArgument(column1);
  const argumentLeft2 = new HavingExpressionArgument(manualExpression1);

  const argumentRight1 = new HavingExpressionArgument(column2);
  const argumentRight2 = new HavingExpressionArgument(manualExpression2);

  it('should compare to another HavingExpression', () => {
    expect(
      new HavingExpression(argumentLeft1, argumentRight1, BoolOperation.Equal).equalsTo(
        new HavingExpression(argumentLeft1, argumentRight1, BoolOperation.Equal)
      )
    ).toEqual(true);
    expect(
      new HavingExpression(argumentLeft1, argumentRight1, BoolOperation.Equal).equalsTo(
        new HavingExpression(argumentLeft1, argumentRight2, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new HavingExpression(argumentLeft1, argumentRight1, BoolOperation.Equal).equalsTo(
        new HavingExpression(argumentLeft2, argumentRight1, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new HavingExpression(argumentLeft1, argumentRight1, BoolOperation.Equal).equalsTo(
        new HavingExpression(argumentLeft1, argumentRight1, BoolOperation.NotEqual)
      )
    ).toEqual(false);
  });

  it('should deserialize', () => {
    const checkDeserialization = (
      left: HavingExpressionArgument,
      right: HavingExpressionArgument,
      op: BoolOperation
    ): void => {
      const havingExpression = new HavingExpression(left, right, op);

      const serializedHavingExpression = havingExpression.serialize();
      const expectedSerializedHavingExpression: HavingExpressionDetails = {
        leftArgument: left.serialize(),
        rightArgument: right.serialize(),
        operation: op,
      };
      expect(serializedHavingExpression).toEqual(expectedSerializedHavingExpression);

      const deserializedHavingExpression = HavingExpression.deserialize(serializedHavingExpression);
      expect(deserializedHavingExpression.equalsTo(havingExpression)).toEqual(true);
    };

    checkDeserialization(argumentLeft1, argumentRight1, BoolOperation.Equal);
    checkDeserialization(argumentLeft1, argumentRight2, BoolOperation.Equal);
    checkDeserialization(argumentLeft2, argumentRight1, BoolOperation.Equal);
    checkDeserialization(argumentLeft2, argumentRight2, BoolOperation.Equal);
  });

  it('should return proper sql', () => {
    expect(new HavingExpression().getSql()).toEqual('');

    const havingExpression1 = new HavingExpression(
      argumentLeft1,
      argumentRight1,
      BoolOperation.NotEqual
    );
    const expectedSql1 = `${argumentLeft1.getSql()} ${
      BoolOperation.NotEqual
    } ${argumentRight1.getSql()}`;
    expect(havingExpression1.getSql()).toEqual(expectedSql1);

    const havingExpression2 = new HavingExpression(
      argumentLeft2,
      argumentRight2,
      BoolOperation.NotEqual
    );
    const expectedSql2 = `${argumentLeft2.getSql()} ${
      BoolOperation.NotEqual
    } ${argumentRight2.getSql()}`;
    expect(havingExpression2.getSql()).toEqual(expectedSql2);
  });
});

describe('HavingClause', () => {
  it('should combine expressions by AND', () => {
    let havingClause = new HavingClause();

    const havingExpression1 = new HavingExpression(
      new HavingExpressionArgument(new Column('COUNT', '', new DbField('testdb', 'tbl', 'a'))),
      new HavingExpressionArgument('10'),
      BoolOperation.LessOrEqual
    );

    const havingExpression2 = new HavingExpression(
      new HavingExpressionArgument(new Column('AVG', '', new DbField('testdb', 'tbl', 'b'))),
      new HavingExpressionArgument('10'),
      BoolOperation.LessOrEqual
    );

    havingClause.add(havingExpression1);
    havingClause.add(havingExpression2);

    expect(havingClause.getSql()).toEqual(
      `\t${havingExpression1.getSql()} AND \n\t${havingExpression2.getSql()}`
    );
  });

  it('should deserialize', () => {
    let havingClause = new HavingClause();

    const havingExpression1 = new HavingExpression(
      new HavingExpressionArgument(new Column('COUNT', '', new DbField('testdb', 'tbl', 'a'))),
      new HavingExpressionArgument('10'),
      BoolOperation.LessOrEqual
    );

    const havingExpression2 = new HavingExpression(
      new HavingExpressionArgument(new Column('AVG', '', new DbField('testdb', 'tbl', 'b'))),
      new HavingExpressionArgument('10'),
      BoolOperation.LessOrEqual
    );

    havingClause.add(havingExpression1);
    havingClause.add(havingExpression2);

    const serializedHavingClause = havingClause.serialize();
    const expectedSerializedHavingClause: HavingExpressionDetails[] = [
      havingExpression1.serialize(),
      havingExpression2.serialize(),
    ];
    expect(serializedHavingClause).toEqual(expectedSerializedHavingClause);

    const deserializedHavingClause = HavingClause.deserialize(serializedHavingClause);
    expect(deserializedHavingClause.equalsTo(havingClause)).toEqual(true);
  });

  it('should remove expression on column remove', () => {
    let havingClause = new HavingClause();

    const havingExpression1 = new HavingExpression(
      new HavingExpressionArgument(new Column('COUNT', '', new DbField('testdb', 'tbl', 'a'))),
      new HavingExpressionArgument('10'),
      BoolOperation.LessOrEqual
    );

    const havingExpression2 = new HavingExpression(
      new HavingExpressionArgument('10'),
      new HavingExpressionArgument(new Column('AVG', '', new DbField('testdb', 'tbl', 'b'))),
      BoolOperation.LessOrEqual
    );

    havingClause.add(havingExpression1);
    havingClause.add(havingExpression2);

    expect(havingClause.childrenCount()).toEqual(2);

    const columnToRemove = new Column('COUNT', '', new DbField('testdb', 'tbl', 'a'));
    havingClause.onSelectStatementColumnRemove(columnToRemove);
    expect(havingClause.childrenCount()).toEqual(1);

    let hasRemovedColumn =
      havingClause.get(0).leftArgument.hasSelectStatementColumn() &&
      havingClause.get(0).leftArgument.selectStatementColumn!.equalsTo(columnToRemove);
    hasRemovedColumn =
      hasRemovedColumn ||
      (havingClause.get(0).rightArgument.hasSelectStatementColumn() &&
        havingClause.get(0).rightArgument.selectStatementColumn!.equalsTo(columnToRemove));
    expect(hasRemovedColumn).toEqual(false);
  });

  it('should update on column change', () => {
    const column1 = new Column('COUNT', '', new DbField('testdb', 'tbl1', 'a'));
    const column2 = new Column('SUM', '', new DbField('testdb', 'tbl2', 'b'));

    let argumentLeft = new HavingExpressionArgument(column1);
    let argumentRight = new HavingExpressionArgument(column2);

    const havingExpression1 = new HavingExpression(
      argumentLeft,
      argumentRight,
      BoolOperation.LessOrEqual
    );

    let havingClause = new HavingClause();
    havingClause.add(havingExpression1);

    const columnToUpdate = new Column('COUNT', '', undefined, 'a + b');
    havingClause.onSelectStatementColumnUpdate(column1, columnToUpdate);
    expect(argumentLeft.hasSelectStatementColumn()).toEqual(true);
    expect(argumentLeft.selectStatementColumn!.equalsTo(columnToUpdate));

    havingClause.onSelectStatementColumnUpdate(column2, columnToUpdate);
    expect(argumentRight.hasSelectStatementColumn()).toEqual(true);
    expect(argumentRight.selectStatementColumn!.equalsTo(columnToUpdate));
  });
});
