import { Subject } from 'rxjs';
import { SqlStatementComponent, SqlStatementComponentList } from './SqlStatement';

/**
 * The base class for observable SQL statement components lists.
 *
 * Observable lists notify their subscribers about events of adding a new component, and
 * deleting/updating an existing component
 */
export class ObservableSqlStatementComponentList<
  ChildDetailsInterface,
  Child extends SqlStatementComponent<ChildDetailsInterface>
> extends SqlStatementComponentList<ChildDetailsInterface, Child> /* istanbul ignore next */ {
  // Note (pavel.yurin@nxlog.org): the "istanbul ignore next" comment is needed to ignore the class
  // in the test coverage report, somehow jest is arguing "branch not covered"
  // on the line "export class ObservableSqlStatementComponentList ...", this seems to be a false
  // positive.
  // (jest uses https://github.com/istanbuljs/istanbuljs library for test coverage)

  private internalAdditionsEvents = new Subject<Child>();
  private internalUpdatesEvents = new Subject<[Child, Child]>();
  private internalDeletionsEvents = new Subject<Child>();

  /**
   * @returns Observable that can be used to subscribe for receiving notifications about adding new element
   */
  get additionsEvents() {
    return this.internalAdditionsEvents.asObservable();
  }
  /**
   * @returns Observable that can be used to subscribe for receiving notifications about updating an existing element
   */
  get updatesEvents() {
    return this.internalUpdatesEvents.asObservable();
  }
  /**
   * @returns Observable that can be used to subscribe for receiving notifications about deleting an existing element
   */
  get deletionsEvents() {
    return this.internalDeletionsEvents.asObservable();
  }

  override add(component: Child): void {
    super.add(component);
    this.internalAdditionsEvents.next(component);
  }
  override update(index: number, component: Child): void {
    const oldItem = this.get(index);
    super.update(index, component);
    this.internalUpdatesEvents.next([oldItem!, component]);
  }
  override remove(index: number): void {
    const itemToRemove = this.get(index);
    super.remove(index);
    this.internalDeletionsEvents.next(itemToRemove);
  }
}
