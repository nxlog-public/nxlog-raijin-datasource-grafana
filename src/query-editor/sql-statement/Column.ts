import { DbField } from '../../DatabaseClient';
import { AggregationKind } from '../components/AggregationSelect';
import { SqlStatementComponent } from './SqlStatement';
import { ObservableSqlStatementComponentList } from './ObservableSqlStatement';

/**
 * Describes the Column class's data representation exposed to Grafana
 *
 * (for detail see {@link Column#serialize}, {@link Column#deserialize}, {@link ColumnList#deserialize})
 */
export interface ColumnDetails {
  aggregation?: AggregationKind;
  alias: string;
  dbField?: DbField;
  manualExpression: string;
}

/**
 * A SELECT statement column.
 *
 * Can be represented by:
 * - a database table field (see {@link DbField}), or
 * - an arbitrary manual expression.
 *
 * Can be aggregated (see {@link AggregationKind}), and can have an alias.
 */
export class Column extends SqlStatementComponent<ColumnDetails> {
  constructor(
    public aggregation?: AggregationKind,
    public alias = '',
    private internalDbField?: DbField,
    private internalManualExpression = ''
  ) {
    super();
    if (this.dbField !== undefined && this.manualExpression !== '') {
      throw new Error('Only one of dbField/manualExpression should be specified');
    }
    if (this.internalDbField === undefined && this.internalManualExpression === '') {
      this.internalDbField = new DbField();
    }
  }

  override serialize(): ColumnDetails {
    return {
      aggregation: this.aggregation,
      alias: this.alias,
      dbField: this.dbField,
      manualExpression: this.manualExpression,
    };
  }

  static deserialize(value: ColumnDetails): Column {
    return new Column(
      value.aggregation,
      value.alias,
      value.dbField === undefined
        ? undefined
        : new DbField(
            value.dbField.databaseName,
            value.dbField.tableName,
            value.dbField.name,
            value.dbField.type
          ),
      value.manualExpression
    );
  }

  override equalsTo(other: Column): boolean {
    return (
      this.aggregation === other.aggregation &&
      this.alias === other.alias &&
      this.internalManualExpression === other.internalManualExpression &&
      ((this.internalDbField === undefined && other.internalDbField === undefined) ||
        (this.internalDbField !== undefined &&
          other.internalDbField !== undefined &&
          this.internalDbField.equalsTo(other.internalDbField)))
    );
  }

  getSqlWithoutAlias(): string {
    let rs = '';
    if (this.internalDbField === undefined) {
      rs = this.internalManualExpression;
    } else if (this.internalDbField.tableName !== '' && this.internalDbField.name !== '') {
      rs = this.internalDbField.getSql();
    }
    if (this.hasAggregation()) {
      rs = `${this.aggregation}(${rs})`;
    }
    return rs;
  }

  override getSql(): string {
    let rs = this.getSqlWithoutAlias();
    if (this.alias !== '') {
      rs = `${rs} AS "${this.alias}"`;
    }
    return rs;
  }

  hasAggregation(): boolean {
    return this.aggregation !== undefined;
  }

  hasDbField(): boolean {
    return this.internalDbField !== undefined;
  }
  get dbField(): DbField | undefined {
    return this.internalDbField;
  }
  set dbField(value: DbField | undefined) {
    this.internalDbField = value;
    this.internalManualExpression = '';
  }

  get manualExpression(): string {
    return this.internalManualExpression;
  }
  set manualExpression(value: string) {
    this.internalManualExpression = value;
    this.internalDbField = undefined;
  }

  hasAlias(): boolean {
    return this.alias !== '';
  }
}

export class ColumnList extends ObservableSqlStatementComponentList<ColumnDetails, Column> {
  static deserialize(value: ColumnDetails[]): ColumnList {
    let rs = new ColumnList();
    value.forEach((obj) => rs.add(Column.deserialize(obj)));
    return rs;
  }
}
