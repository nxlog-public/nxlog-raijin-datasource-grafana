import { SqlStatementComponent, SqlStatementComponentList } from './SqlStatement';
import { Column, ColumnDetails } from './Column';
import { DbField } from '../../DatabaseClient';

/**
 * Describes the GroupByExpression class's data representation exposed to Grafana
 *
 * (for details see {@link GroupByExpression#serialize}, {@link GroupByExpression#deserialize},
 * {@link GroupByClause#deserialize})
 */
export interface GroupByExpressionDetails {
  selectStatementField?: ColumnDetails;
  tableField?: DbField;
  manualExpression: string;
}

/**
 * A single expression of a {@link GroupByClause|GROUP BY clause}
 *
 * Can be represented by:
 * - a SELECT statement column (see {@link Column}),
 * - a database table field (see {@link DbField}),
 * - an arbitrary manual expression
 */
export class GroupByExpression extends SqlStatementComponent<GroupByExpressionDetails> {
  private internalSelectStatementColumn?: Column;
  private internalTableField?: DbField;
  private internalManualExpression = '';

  constructor(field?: Column | DbField | string) {
    super();
    if (field === undefined) {
      this.internalSelectStatementColumn = new Column();
    } else if (field instanceof Column) {
      this.internalSelectStatementColumn = field as Column;
    } else if (field instanceof DbField) {
      this.internalTableField = field as DbField;
    } else {
      this.internalManualExpression = field as string;
    }
  }

  override equalsTo(other: GroupByExpression): boolean {
    const selectStatementFieldsEqual =
      (this.internalSelectStatementColumn === undefined &&
        other.internalSelectStatementColumn === undefined) ||
      (this.internalSelectStatementColumn !== undefined &&
        other.internalSelectStatementColumn !== undefined &&
        this.internalSelectStatementColumn.equalsTo(other.internalSelectStatementColumn));
    const tableFieldsEqual =
      (this.internalTableField === undefined && other.internalTableField === undefined) ||
      (this.internalTableField !== undefined &&
        other.internalTableField !== undefined &&
        this.internalTableField.equalsTo(other.internalTableField));
    return (
      selectStatementFieldsEqual &&
      tableFieldsEqual &&
      this.internalManualExpression === other.internalManualExpression
    );
  }

  override serialize(): GroupByExpressionDetails {
    return {
      selectStatementField: this.internalSelectStatementColumn?.serialize(),
      tableField: this.internalTableField,
      manualExpression: this.internalManualExpression,
    };
  }

  static deserialize(value: GroupByExpressionDetails): GroupByExpression {
    if (value.selectStatementField !== undefined) {
      return new GroupByExpression(Column.deserialize(value.selectStatementField));
    } else if (value.tableField !== undefined) {
      return new GroupByExpression(
        new DbField(
          value.tableField.databaseName,
          value.tableField.tableName,
          value.tableField.name,
          value.tableField.type
        )
      );
    } else {
      return new GroupByExpression(value.manualExpression);
    }
  }

  override getSql(): string {
    if (this.internalSelectStatementColumn !== undefined) {
      return this.internalSelectStatementColumn.getSqlWithoutAlias();
    } else if (this.internalTableField !== undefined) {
      return this.internalTableField.getSql();
    }
    return this.internalManualExpression;
  }

  hasSelectStatementColumn(): boolean {
    return this.internalSelectStatementColumn !== undefined;
  }
  get selectStatementColumn(): Column | undefined {
    return this.internalSelectStatementColumn;
  }
  set selectStatementColumn(value: Column | undefined) {
    this.reset();
    this.internalSelectStatementColumn = value;
  }

  hasTableField(): boolean {
    return this.internalTableField !== undefined;
  }
  get tableField(): DbField | undefined {
    return this.internalTableField;
  }
  set tableField(value: DbField | undefined) {
    this.reset();
    this.internalTableField = value;
  }

  get manualExpression(): string {
    return this.internalManualExpression;
  }
  set manualExpression(value: string) {
    this.reset();
    this.internalManualExpression = value;
  }

  private reset(): void {
    this.internalSelectStatementColumn = undefined;
    this.internalTableField = undefined;
    this.internalManualExpression = '';
  }
}

export class GroupByClause extends SqlStatementComponentList<
  GroupByExpressionDetails,
  GroupByExpression
> {
  static deserialize(value: GroupByExpressionDetails[]): GroupByClause {
    let rs = new GroupByClause();
    value.forEach((obj) => rs.add(GroupByExpression.deserialize(obj)));
    return rs;
  }
  onSelectStatementColumnRemove(column: Column) {
    this.forEach((groupByExpression, index) => {
      if (
        groupByExpression.hasSelectStatementColumn() &&
        groupByExpression.selectStatementColumn!.equalsTo(column)
      ) {
        this.remove(index);
      }
    });
  }
  onSelectStatementColumnUpdate(oldColumn: Column, newColumn: Column) {
    this.forEach((groupByExpression, index) => {
      if (
        groupByExpression.hasSelectStatementColumn() &&
        groupByExpression.selectStatementColumn!.equalsTo(oldColumn)
      ) {
        if (newColumn.hasDbField() && newColumn.dbField!.isAsterisk()) {
          this.remove(index);
        } else {
          groupByExpression.selectStatementColumn = newColumn;
        }
      }
    });
  }
}
