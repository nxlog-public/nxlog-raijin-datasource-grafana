import { Column, ColumnDetails, ColumnList } from './Column';
import { DbField } from '../../DatabaseClient';
import { AggregationKind } from '../components/AggregationSelect';

describe('Column', () => {
  it('should represent DbField by default', () => {
    const column = new Column();
    expect(column.hasDbField()).toEqual(true);
  });

  it('should throw out exception on specifying both dbField and manualExpression', () => {
    const expectedErrorText = 'Only one of dbField/manualExpression should be specified';
    expect(() => new Column('AVG', '', new DbField(), 'a + b')).toThrow(expectedErrorText);
  });

  it('should clear dbfield after setting the manual expression', () => {
    let column = new Column(
      undefined, // aggregation
      '',
      new DbField('testdb', 't1', 'a', 'int')
    );

    expect(column.hasDbField()).toEqual(true);

    column.manualExpression = '(a + b)';
    expect(column.hasDbField()).toEqual(false);
    expect(column.dbField).toBeUndefined();
  });

  it('should clear manual expression after setting the dbfield', () => {
    let column = new Column(
      undefined, // aggregation
      '',
      undefined, // dbfield
      '(a + b)'
    );

    expect(column.hasDbField()).toEqual(false);

    column.dbField = new DbField('testdb', 't1', 'a', 'int');
    expect(column.manualExpression).toEqual('');
    expect(column.hasDbField()).toEqual(true);
  });

  it('should return proper sql for dbfield', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const column = new Column(undefined, '', dbField, '');

    // being represented by DbField, Column.getSql()/getSqlWithoutAlias() methods should return
    // the same as what DbField.getSql() does
    expect(column.getSqlWithoutAlias()).toEqual(dbField.getSql());
    expect(column.getSql()).toEqual(dbField.getSql());
  });

  it('should return proper sql for dbfield with alias', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const column = new Column(undefined, 'foo', dbField, '');

    // being represented by DbField, Column.getSqlWithoutAlias() should return the same as what
    // DbField.getSql() does; Column.getSql() should add the alias
    expect(column.getSqlWithoutAlias()).toEqual(dbField.getSql());
    expect(column.getSql()).toEqual(`${dbField.getSql()} AS "foo"`);
  });

  it('should return proper sql for dbfield with alias and aggregation', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const column = new Column('COUNT', 'foo', dbField, '');

    // being represented by DbField and aggregation, Column.getSqlWithoutAlias() should return
    // "aggregation(DbField.getSql())"; Column.getSql() should add the alias
    expect(column.getSqlWithoutAlias()).toEqual(`COUNT(${dbField.getSql()})`);
    expect(column.getSql()).toEqual(`COUNT(${dbField.getSql()}) AS "foo"`);
  });

  it('should return proper sql for manual expression', () => {
    const column = new Column(undefined, '', undefined, 'a + b');

    // being represented by the manual expression, Column.getSqlWithoutAlias()/getSql() should
    // return that manual expression
    expect(column.getSqlWithoutAlias()).toEqual('a + b');
    expect(column.getSql()).toEqual('a + b');
  });

  it('should return proper sql for manual expression with alias', () => {
    const column = new Column(undefined, 'foo', undefined, 'a + b');

    // being represented by the manual expression, Column.getSqlWithoutAlias() should
    // return that manual expression; Column.getSql() should add the alias
    expect(column.getSqlWithoutAlias()).toEqual('a + b');
    expect(column.getSql()).toEqual(`a + b AS "foo"`);
  });

  it('should return proper sql for manual expression with alias and aggregation', () => {
    const column = new Column('COUNT', 'foo', undefined, 'a + b');

    // being represented by the manual expression and aggregation, Column.getSqlWithoutAlias()
    // should return "aggregation(<manual expression>)"; Column.getSql() should add the alias
    expect(column.getSqlWithoutAlias()).toEqual('COUNT(a + b)');
    expect(column.getSql()).toEqual(`COUNT(a + b) AS "foo"`);
  });

  it('should properly deserialize serialized column', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const alias = 'foo';
    const aggregation = 'COUNT';
    const manualExpression = 'a + b';

    const testColumnDeserialization = (
      aggregation: AggregationKind | undefined,
      alias: string,
      dbField: DbField | undefined,
      manualExpression: string
    ): void => {
      const column = new Column(aggregation, alias, dbField, manualExpression);
      const serializedColumn = column.serialize();

      const expectedSerializedColumn: ColumnDetails = {
        aggregation: aggregation,
        alias: alias,
        dbField: dbField,
        manualExpression: manualExpression,
      };

      expect(serializedColumn).toEqual(expectedSerializedColumn);

      const deserializedColumn = Column.deserialize(serializedColumn);
      expect(deserializedColumn.equalsTo(column)).toEqual(true);
      expect(deserializedColumn.hasDbField()).toEqual(dbField !== undefined);
      expect(deserializedColumn.hasAggregation()).toEqual(aggregation !== undefined);
      expect(deserializedColumn.hasAlias()).toEqual(alias !== '');
    };

    testColumnDeserialization(undefined, '', dbField, '');
    testColumnDeserialization(aggregation, alias, dbField, '');
    testColumnDeserialization(undefined, '', undefined, manualExpression);
    testColumnDeserialization(aggregation, alias, undefined, manualExpression);
  });

  it('should compare to another column', () => {
    const dbField1 = new DbField('testdb', 't1', 'a', 'int');
    const dbField2 = new DbField('testdb', 't1', 'b', 'int');

    const manualExpression1 = 'a + b';
    const manualExpression2 = 'a - b';

    const alias1 = 'foo';
    const alias2 = 'bar';

    const aggregation1: AggregationKind = 'COUNT';
    const aggregation2: AggregationKind = 'AVG';

    const columnField = new Column(aggregation1, alias1, dbField1);

    expect(columnField.equalsTo(new Column(aggregation1, alias1, dbField1))).toEqual(true);

    expect(columnField.equalsTo(new Column(undefined, alias1, dbField1))).toEqual(false);
    expect(columnField.equalsTo(new Column(aggregation2, alias1, dbField1))).toEqual(false);
    expect(columnField.equalsTo(new Column(aggregation1, alias2, dbField1))).toEqual(false);
    expect(columnField.equalsTo(new Column(aggregation1, alias1, dbField2))).toEqual(false);
    expect(
      columnField.equalsTo(new Column(aggregation1, alias1, undefined, manualExpression1))
    ).toEqual(false);

    const columnManual = new Column(aggregation1, alias1, undefined, manualExpression1);

    expect(
      columnManual.equalsTo(new Column(aggregation1, alias1, undefined, manualExpression1))
    ).toEqual(true);

    expect(
      columnManual.equalsTo(new Column(undefined, alias1, undefined, manualExpression1))
    ).toEqual(false);
    expect(
      columnManual.equalsTo(new Column(aggregation2, alias1, undefined, manualExpression1))
    ).toEqual(false);
    expect(
      columnManual.equalsTo(new Column(aggregation1, alias2, undefined, manualExpression1))
    ).toEqual(false);
    expect(
      columnManual.equalsTo(new Column(aggregation1, alias1, undefined, manualExpression2))
    ).toEqual(false);
    expect(columnManual.equalsTo(new Column(aggregation1, alias1, dbField1))).toEqual(false);
  });
});

describe('ColumnList', () => {
  let columnList: ColumnList;

  beforeEach(() => {
    columnList = new ColumnList();
    columnList.add(new Column(undefined, '', new DbField('testdb', 'tbl', 'a')));
    columnList.add(new Column(undefined, '', new DbField('testdb', 'tbl', 'b')));
    columnList.add(new Column(undefined, '', new DbField('testdb', 'tbl', 'c')));
  });

  it('should combine columns by comma', () => {
    expect(columnList.getSql()).toEqual('\t"tbl"."a",\n\t"tbl"."b",\n\t"tbl"."c"');
  });

  it('should throw out index out of range', () => {
    expect(() => columnList.get(-1)).toThrow('Index is out of range');
    expect(() => columnList.get(3)).toThrow('Index is out of range');
  });
});
