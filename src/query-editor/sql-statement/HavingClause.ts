import { SqlStatementComponent, SqlStatementComponentList } from './SqlStatement';
import { Column, ColumnDetails } from './Column';
import { BoolOperation, FormatBoolOperationArgument } from './BoolOperation';
import assert from 'assert';
import { isString } from 'lodash';

/**
 * Describes the HavingExpressionArgument data representation exposed to Grafana
 *
 * (for details see {@link HavingExpressionArgument#serialize}, {@link HavingExpressionArgument#deserialize})
 */
export interface HavingExpressionArgumentDetails {
  selectStatementColumn?: ColumnDetails;
  manualExpression: string;
}

/**
 * A {@link HavingExpression|HAVING expression} argument
 *
 * Can be represented by:
 * - a SELECT statement {@link Column|column},
 * - an arbitrary manual expression
 */
export class HavingExpressionArgument extends SqlStatementComponent<HavingExpressionArgumentDetails> {
  private internalSelectStatementColumn?: Column;
  private internalManualExpression = '';

  constructor(field?: Column | string) {
    super();
    if (field === undefined) {
      this.internalSelectStatementColumn = new Column();
    } else if (field instanceof Column) {
      this.internalSelectStatementColumn = field as Column;
    } else {
      assert(isString(field));
      this.internalManualExpression = field as string;
    }
  }

  override equalsTo(other: HavingExpressionArgument): boolean {
    const columnsEqual =
      (this.internalSelectStatementColumn === undefined &&
        other.internalSelectStatementColumn === undefined) ||
      (this.internalSelectStatementColumn !== undefined &&
        other.internalSelectStatementColumn !== undefined &&
        this.internalSelectStatementColumn.equalsTo(other.internalSelectStatementColumn));
    return columnsEqual && this.internalManualExpression === other.internalManualExpression;
  }

  override serialize(): HavingExpressionArgumentDetails {
    return {
      selectStatementColumn: this.internalSelectStatementColumn?.serialize(),
      manualExpression: this.internalManualExpression,
    };
  }

  static deserialize(value: HavingExpressionArgumentDetails): HavingExpressionArgument {
    if (value.selectStatementColumn !== undefined) {
      return new HavingExpressionArgument(Column.deserialize(value.selectStatementColumn));
    } else {
      return new HavingExpressionArgument(value.manualExpression);
    }
  }

  override getSql(): string {
    if (this.internalSelectStatementColumn !== undefined) {
      return this.internalSelectStatementColumn.getSqlWithoutAlias();
    }
    return this.internalManualExpression;
  }

  hasSelectStatementColumn(): boolean {
    return this.internalSelectStatementColumn !== undefined;
  }
  get selectStatementColumn(): Column | undefined {
    return this.internalSelectStatementColumn;
  }
  set selectStatementColumn(value: Column | undefined) {
    this.internalSelectStatementColumn = value;
    this.internalManualExpression = '';
  }

  get manualExpression(): string {
    return this.internalManualExpression;
  }
  set manualExpression(value: string) {
    this.internalManualExpression = value;
    this.internalSelectStatementColumn = undefined;
  }
}

/**
 * Describes the HavingExpression class's data representation exposed to Grafana
 *
 * (for details see {@link HavingExpression#serialize}, {@link HavingExpression#deserialize},
 * {@link HavingClause#deserialize})
 */
export interface HavingExpressionDetails {
  leftArgument: HavingExpressionArgumentDetails;
  rightArgument: HavingExpressionArgumentDetails;
  operation?: BoolOperation;
}

export class HavingExpression extends SqlStatementComponent<HavingExpressionDetails> {
  constructor(
    public leftArgument: HavingExpressionArgument = new HavingExpressionArgument(),
    public rightArgument: HavingExpressionArgument = new HavingExpressionArgument(),
    public operation?: BoolOperation
  ) {
    super();
  }

  override equalsTo(other: HavingExpression): boolean {
    return (
      this.leftArgument.equalsTo(other.leftArgument) &&
      this.rightArgument.equalsTo(other.rightArgument) &&
      this.operation === other.operation
    );
  }

  override serialize(): HavingExpressionDetails {
    return {
      leftArgument: this.leftArgument.serialize(),
      rightArgument: this.rightArgument.serialize(),
      operation: this.operation,
    };
  }

  static deserialize(value: HavingExpressionDetails): HavingExpression {
    return new HavingExpression(
      HavingExpressionArgument.deserialize(value.leftArgument),
      HavingExpressionArgument.deserialize(value.rightArgument),
      value.operation
    );
  }

  override getSql(): string {
    if (
      this.leftArgument === undefined ||
      this.rightArgument === undefined ||
      this.operation === undefined
    ) {
      return '';
    }
    return `${this.leftArgument.getSql()} ${this.operation} ${FormatBoolOperationArgument(
      this.operation,
      this.rightArgument.getSql()
    )}`;
  }
}

export class HavingClause extends SqlStatementComponentList<
  HavingExpressionDetails,
  HavingExpression
> {
  static deserialize(value: HavingExpressionDetails[]): HavingClause {
    let rs = new HavingClause();
    value.forEach((obj) => rs.add(HavingExpression.deserialize(obj)));
    return rs;
  }
  onSelectStatementColumnRemove(column: Column) {
    this.forEach((havingExpression, index) => {
      if (
        (havingExpression.leftArgument.hasSelectStatementColumn() &&
          havingExpression.leftArgument.selectStatementColumn!.equalsTo(column)) ||
        (havingExpression.rightArgument.hasSelectStatementColumn() &&
          havingExpression.rightArgument.selectStatementColumn!.equalsTo(column))
      ) {
        this.remove(index);
      }
    });
  }
  onSelectStatementColumnUpdate(oldColumn: Column, newColumn: Column) {
    this.forEach((havingExpression) => {
      if (
        havingExpression.leftArgument.hasSelectStatementColumn() &&
        havingExpression.leftArgument.selectStatementColumn!.equalsTo(oldColumn)
      ) {
        havingExpression.leftArgument.selectStatementColumn = newColumn;
      }
      if (
        havingExpression.rightArgument.hasSelectStatementColumn() &&
        havingExpression.rightArgument.selectStatementColumn!.equalsTo(oldColumn)
      ) {
        havingExpression.rightArgument.selectStatementColumn = newColumn;
      }
    });
  }

  protected override separator(): string {
    return ' AND ';
  }
}
