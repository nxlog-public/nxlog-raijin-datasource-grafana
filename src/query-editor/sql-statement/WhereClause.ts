import { SqlStatementComponent, SqlStatementComponentList } from './SqlStatement';
import { BoolOperation, FormatBoolOperationArgument } from './BoolOperation';
import { DbField } from '../../DatabaseClient';
import { isString } from 'lodash';
import assert from 'assert';

export class GrafanaVariable {
  // istanbul ignore next
  constructor(public name: string = '') {}
}

/**
 * Describes the WhereExpressionArgument data representation exposed to Grafana
 *
 * (for details see {@link WhereExpressionArgument#serialize}, {@link WhereExpressionArgument#deserialize})
 */
export interface WhereExpressionArgumentDetails {
  dbField?: DbField;
  manualExpression?: string;
  grafanaVariable?: GrafanaVariable;
}
/**
 * An argument of a single expression of the WHERE clause
 *
 * Can be represented by:
 * - a database table field (see {@link DbField}),
 * - an arbitrary expression, or
 * - a Grafana variable.
 */
export class WhereExpressionArgument extends SqlStatementComponent<WhereExpressionArgumentDetails> {
  private internalDbField?: DbField;
  private internalManualExpression?: string;
  private internalGrafanaVariable?: GrafanaVariable;

  constructor(field?: DbField | string | GrafanaVariable) {
    super();
    if (field === undefined) {
      this.internalDbField = new DbField();
    } else if (field instanceof DbField) {
      this.internalDbField = field as DbField;
    } else if (field instanceof GrafanaVariable) {
      this.internalGrafanaVariable = field as GrafanaVariable;
    } else {
      assert(isString(field));
      this.internalManualExpression = field as string;
    }
  }

  override equalsTo(other: WhereExpressionArgument): boolean {
    const dbFieldsEqual =
      (this.internalDbField === undefined && other.internalDbField === undefined) ||
      (this.internalDbField !== undefined &&
        other.internalDbField !== undefined &&
        this.internalDbField.equalsTo(other.internalDbField));
    const manualExpressionsEqual = this.internalManualExpression === other.internalManualExpression;
    const grafanaVariablesEqual =
      (this.internalGrafanaVariable === undefined && other.internalGrafanaVariable === undefined) ||
      (this.internalGrafanaVariable !== undefined &&
        other.internalGrafanaVariable !== undefined &&
        this.internalGrafanaVariable.name === other.internalGrafanaVariable.name);
    return dbFieldsEqual && manualExpressionsEqual && grafanaVariablesEqual;
  }

  override serialize(): WhereExpressionArgumentDetails {
    return {
      dbField: this.internalDbField,
      manualExpression: this.internalManualExpression,
      grafanaVariable: this.internalGrafanaVariable,
    };
  }

  static deserialize(value: WhereExpressionArgumentDetails): WhereExpressionArgument {
    if (value.dbField !== undefined) {
      return new WhereExpressionArgument(
        new DbField(
          value.dbField.databaseName,
          value.dbField.tableName,
          value.dbField.name,
          value.dbField.type
        )
      );
    } else if (value.manualExpression !== undefined) {
      return new WhereExpressionArgument(value.manualExpression);
    } else {
      assert(value.grafanaVariable !== undefined);
      return new WhereExpressionArgument(new GrafanaVariable(value.grafanaVariable.name));
    }
  }

  override getSql(): string {
    if (this.internalDbField !== undefined) {
      return this.internalDbField.getSql();
    } else if (this.internalManualExpression !== undefined) {
      return this.internalManualExpression;
    } else {
      return `\$${this.internalGrafanaVariable!.name}`;
    }
  }

  hasDbField(): boolean {
    return this.internalDbField !== undefined;
  }
  get dbField(): DbField | undefined {
    return this.internalDbField;
  }
  set dbField(value: DbField | undefined) {
    this.reset();
    this.internalDbField = value;
  }

  hasManualExpression(): boolean {
    return this.internalManualExpression !== undefined;
  }
  get manualExpression(): string | undefined {
    return this.internalManualExpression;
  }
  set manualExpression(value: string | undefined) {
    this.reset();
    this.internalManualExpression = value;
  }

  get grafanaVariable(): GrafanaVariable | undefined {
    return this.internalGrafanaVariable;
  }
  set grafanaVariable(value: GrafanaVariable | undefined) {
    this.reset();
    this.internalGrafanaVariable = value;
  }

  private reset(): void {
    this.internalDbField = undefined;
    this.internalManualExpression = undefined;
    this.internalGrafanaVariable = undefined;
  }
}

/**
 * Describes the WhereExpression data representation exposed to Grafana
 *
 * (for details see {@link WhereExpression#serialize}, {@link WhereExpression#deserialize},
 * {@link WhereClause#deserialize})
 */
export interface WhereExpressionDetails {
  leftExpression?: WhereExpressionArgumentDetails;
  rightExpression?: WhereExpressionArgumentDetails;
  operation?: BoolOperation;
}

export class WhereExpression extends SqlStatementComponent<WhereExpressionDetails> {
  constructor(
    public leftExpression?: WhereExpressionArgument,
    public rightExpression?: WhereExpressionArgument,
    public operation?: BoolOperation
  ) {
    super();
  }

  override equalsTo(other: WhereExpression): boolean {
    const compareExpressions = (e1?: WhereExpressionArgument, e2?: WhereExpressionArgument) => {
      return (
        (e1 === undefined && e2 === undefined) ||
        (e1 !== undefined && e2 !== undefined && e1.equalsTo(e2))
      );
    };
    const leftExpressionEqual = compareExpressions(this.leftExpression, other.leftExpression);
    const rightExpressionEqual = compareExpressions(this.rightExpression, other.rightExpression);
    return leftExpressionEqual && rightExpressionEqual && this.operation === other.operation;
  }

  override serialize(): WhereExpressionDetails {
    return {
      leftExpression: this.leftExpression?.serialize(),
      rightExpression: this.rightExpression?.serialize(),
      operation: this.operation,
    };
  }

  static deserialize(value: WhereExpressionDetails): WhereExpression {
    return new WhereExpression(
      value.leftExpression === undefined
        ? undefined
        : WhereExpressionArgument.deserialize(value.leftExpression),
      value.rightExpression === undefined
        ? undefined
        : WhereExpressionArgument.deserialize(value.rightExpression),
      value.operation
    );
  }

  override getSql(): string {
    if (
      this.leftExpression === undefined ||
      this.rightExpression === undefined ||
      this.operation === undefined
    ) {
      return '';
    }
    return `${this.leftExpression.getSql()} ${this.operation} ${FormatBoolOperationArgument(
      this.operation,
      this.rightExpression.getSql()
    )}`;
  }
}

export class WhereClause extends SqlStatementComponentList<
  WhereExpressionDetails,
  WhereExpression
> {
  static deserialize(value: WhereExpressionDetails[]): WhereClause {
    let rs = new WhereClause();
    value.forEach((obj) => rs.add(WhereExpression.deserialize(obj)));
    return rs;
  }

  protected override separator(): string {
    return ' AND ';
  }
}
