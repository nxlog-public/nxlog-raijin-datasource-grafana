import { Subscription } from 'rxjs';
import { FromClause, FromClauseDetails } from './FromClause';
import { Column, ColumnDetails, ColumnList } from './Column';
import { WhereClause, WhereExpressionDetails } from './WhereClause';
import { GroupByClause, GroupByExpression, GroupByExpressionDetails } from './GroupByClause';
import { HavingClause, HavingExpressionDetails } from './HavingClause';
import { TimeRange } from '@grafana/data';
import { TimeColumn, TimeColumnDetails } from './TimeColumn';
import { SqlStatementComponent } from './SqlStatement';
import assert from 'assert';

/**
 * Describes the SelectStatement class's data representation exposed to Grafana
 * (data in this format is passed to, and received from, the Grafana internals,
 * see {@link SelectStatement#serialize}, {@link SelectStatement#deserialize})
 */
export interface SelectStatementDetails {
  fromClause: FromClauseDetails;
  selectColumns: ColumnDetails[];
  whereClause: WhereExpressionDetails[];
  groupByClause: GroupByExpressionDetails[];
  havingClause: HavingExpressionDetails[];
  timeColumn: TimeColumnDetails;
  synchronizedGroupByColumnsList: GroupByExpressionDetails[];
}

export class SelectStatement extends SqlStatementComponent<SelectStatementDetails> {
  private internalSyncronizeColumns = true;
  private subscription?: Subscription = undefined;

  constructor(
    public fromClause = new FromClause(),
    public selectColumns = new ColumnList(),
    public whereClause = new WhereClause(),
    public groupByClause = new GroupByClause(),
    public havingClause = new HavingClause(),
    public timeColumn = new TimeColumn(),
    private synchronizedGroupByColumnsList = new GroupByClause()
  ) {
    super();
    this.enableEventProcessing();
  }

  override serialize(): SelectStatementDetails {
    return {
      fromClause: this.fromClause.serialize(),
      selectColumns: this.selectColumns.serialize(),
      whereClause: this.whereClause.serialize(),
      groupByClause: this.groupByClause.serialize(),
      havingClause: this.havingClause.serialize(),
      timeColumn: this.timeColumn.serialize(),
      synchronizedGroupByColumnsList: this.synchronizedGroupByColumnsList.serialize(),
    };
  }

  static deserialize(value?: SelectStatementDetails): SelectStatement {
    if (value === undefined) {
      return new SelectStatement();
    }
    return new SelectStatement(
      value.fromClause === undefined ? undefined : FromClause.deserialize(value.fromClause),
      value.selectColumns === undefined ? undefined : ColumnList.deserialize(value.selectColumns),
      value.whereClause === undefined ? undefined : WhereClause.deserialize(value.whereClause),
      value.groupByClause === undefined
        ? undefined
        : GroupByClause.deserialize(value.groupByClause),
      value.havingClause === undefined ? undefined : HavingClause.deserialize(value.havingClause),
      value.timeColumn === undefined ? undefined : TimeColumn.deserialize(value.timeColumn),
      value.synchronizedGroupByColumnsList === undefined
        ? undefined
        : GroupByClause.deserialize(value.synchronizedGroupByColumnsList)
    );
  }

  override equalsTo(other: SelectStatement): boolean {
    return (
      this.fromClause.equalsTo(other.fromClause) &&
      this.selectColumns.equalsTo(other.selectColumns) &&
      this.whereClause.equalsTo(other.whereClause) &&
      this.groupByClause.equalsTo(other.groupByClause) &&
      this.havingClause.equalsTo(other.havingClause) &&
      this.timeColumn.equalsTo(other.timeColumn)
    );
  }

  tearDown() {
    this.disableEventProcessing();
  }

  // Initializes event listeners for auto updating of groupByClause/havingClause in a case if they
  // contain a reference to a select statement column, and for autofilling the groupByClause with
  // non-aggregated fields
  private enableEventProcessing() {
    this.subscription = this.selectColumns.additionsEvents.subscribe(() =>
      this.synchronizeGroupByClause()
    );
    this.subscription.add(
      this.selectColumns.updatesEvents.subscribe((changedCol: [Column, Column]) => {
        this.synchronizeGroupByClause();
        const [oldCol, newCol] = changedCol;
        this.groupByClause.onSelectStatementColumnUpdate(oldCol, newCol);
        this.havingClause.onSelectStatementColumnUpdate(oldCol, newCol);
      })
    );
    this.subscription.add(
      this.selectColumns.deletionsEvents.subscribe((deletedColumn: Column) => {
        this.synchronizeGroupByClause();
        this.groupByClause.onSelectStatementColumnRemove(deletedColumn);
        this.havingClause.onSelectStatementColumnRemove(deletedColumn);
      })
    );
    this.subscription.add(
      this.timeColumn.updatesEvents.subscribe((_: TimeColumn) => {
        this.synchronizeGroupByClause();
      })
    );

    this.synchronizeGroupByClause();
  }

  private disableEventProcessing() {
    assert(this.subscription !== undefined, 'SelectStatement.subscription should be defined');
    this.subscription.unsubscribe();
    this.clearSynchronizedGroupByColumns();
  }

  private selectColumnsContainAggregation(): boolean {
    let rs = false;
    this.selectColumns.forEach((column) => (rs = rs || column.hasAggregation()));
    return rs;
  }

  private addToSynchronizedGroupByColumns(expression: GroupByExpression) {
    this.groupByClause.add(expression);
    this.synchronizedGroupByColumnsList.add(expression);
  }
  private clearSynchronizedGroupByColumns() {
    this.groupByClause.assign(
      this.groupByClause.filter((groupByExpression) => {
        const isExpressionSynchronized =
          this.synchronizedGroupByColumnsList.find((synchronizedExpression) =>
            synchronizedExpression.equalsTo(groupByExpression)
          ) !== undefined;
        return !isExpressionSynchronized;
      })
    );
    this.synchronizedGroupByColumnsList.clear();
  }

  private synchronizeGroupByClause() {
    this.clearSynchronizedGroupByColumns();
    if (this.selectColumnsContainAggregation()) {
      const hasNotGroupedTimeColumn =
        this.timeColumn.enabled &&
        !this.timeColumn.autogenerated &&
        !this.timeColumn.groupByTimeInterval;
      // Add time column
      if (hasNotGroupedTimeColumn) {
        const hasColumn =
          this.groupByClause.find((groupByExpression) => {
            return (
              groupByExpression.hasTableField() &&
              groupByExpression.tableField!.equalsTo(this.timeColumn.field!)
            );
          }) !== undefined;
        if (!hasColumn) {
          this.addToSynchronizedGroupByColumns(new GroupByExpression(this.timeColumn.field!));
        }
      }
      // Add all non-aggregated columns
      this.selectColumns
        .filter((column) => !column.hasAggregation())
        .forEach((column) => {
          const hasColumn =
            this.groupByClause.find((groupByExpression) => {
              return (
                groupByExpression.hasSelectStatementColumn() &&
                groupByExpression.selectStatementColumn!.equalsTo(column)
              );
            }) !== undefined;
          if (!hasColumn) {
            this.addToSynchronizedGroupByColumns(new GroupByExpression(column));
          }
        });
    }
  }

  override getSql(timeRange?: TimeRange): string {
    const targetsSql: string = [this.timeColumn.getFieldsSql(), this.selectColumns.getSql()]
      .filter((s) => s !== '')
      .join(',\n');
    let rs = `SELECT\n${targetsSql}\nFROM\n${this.fromClause.getSql()}`;

    let timeRangeWhere = '';
    if (this.timeColumn.enabled && !this.timeColumn.autogenerated && timeRange !== undefined) {
      const timeFieldName = this.timeColumn.field?.getSql();
      timeRangeWhere = `\t${timeFieldName} >= '${timeRange.from.toISOString()}' AND ${timeFieldName} <= '${timeRange.to.toISOString()}'`;
    }
    const whereSql = [timeRangeWhere, this.whereClause.getSql()]
      .filter((s) => s !== '')
      .join(' AND\n');
    if (whereSql !== '') {
      rs += `\nWHERE\n${whereSql}`;
    }

    const groupBySql: string = [this.timeColumn.getGroupBySql(), this.groupByClause.getSql()]
      .filter((s) => s !== '')
      .join(',\n');
    if (groupBySql !== '') {
      rs += `\nGROUP BY\n${groupBySql}`;
    }
    if (this.havingClause.childrenCount() > 0) {
      rs += `\nHAVING\n${this.havingClause.getSql()}`;
    }
    return rs;
  }

  reset(): void {
    this.fromClause.databaseName = '';
    this.fromClause.tableName = '';
    this.selectColumns.clear();
    this.whereClause.clear();
    this.groupByClause.clear();
    this.havingClause.clear();
    this.timeColumn.reset();
  }

  hasChildren(): boolean {
    return (
      this.selectColumns.childrenCount() > 0 ||
      this.whereClause.childrenCount() > 0 ||
      this.groupByClause.childrenCount() > 0 ||
      this.havingClause.childrenCount() > 0
    );
  }

  get syncronizeColumns(): boolean {
    return this.internalSyncronizeColumns;
  }
  set syncronizeColumns(value: boolean) {
    this.internalSyncronizeColumns = value;
    if (this.internalSyncronizeColumns) {
      this.enableEventProcessing();
    } else {
      this.disableEventProcessing();
    }
  }
}
