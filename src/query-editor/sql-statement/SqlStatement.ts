import { TimeRange } from '@grafana/data';

/** The base class for all SQL statement components (the FROM clause, column, etc.) */
export abstract class SqlStatementComponent<DataDetails> {
  abstract getSql(timeRange?: TimeRange): string;
  abstract serialize(): DataDetails;
  abstract equalsTo(other: SqlStatementComponent<DataDetails>): boolean;
}

/** The base class for SQL statement components lists (list of columns, where expressions, etc.) */
export class SqlStatementComponentList<
  ChildDetails,
  Child extends SqlStatementComponent<ChildDetails>
> extends SqlStatementComponent<ChildDetails[]> {
  private components: Child[] = [];

  override serialize(): ChildDetails[] {
    return this.components.map((component) => component.serialize());
  }

  override equalsTo(other: SqlStatementComponentList<ChildDetails, Child>): boolean {
    if (this.components.length !== other.components.length) {
      return false;
    }
    let equal = true;
    for (let i = 0; i < this.components.length; ++i) {
      equal = equal && this.components[i]!.equalsTo(other.components[i]!);
    }
    return equal;
  }

  protected separator(): string {
    return ',';
  }

  override getSql(): string {
    return this.map((component) => component.getSql())
      .filter((sql) => sql !== '')
      .map((sql) => `\t${sql}`)
      .join(`${this.separator()}\n`);
  }

  childrenCount(): number {
    return this.components.length;
  }

  assign(list: Child[]) {
    this.components = list;
  }
  get(index: number): Child {
    if (index < 0 || index >= this.components.length) {
      throw new Error('Index is out of range');
    }
    return this.components[index] as Child;
  }

  add(component: Child): void {
    this.components.push(component);
  }
  update(index: number, component: Child): void {
    this.components[index] = component;
  }
  remove(index: number): void {
    this.components = this.components.filter(
      (_component, componentIndex) => componentIndex !== index
    );
  }

  map<U>(callbackfn: (value: Child, index: number, array: Child[]) => U): U[] {
    return this.components.map(callbackfn);
  }
  forEach(callbackfn: (value: Child, index: number, array: Child[]) => void, thisArg?: any): void {
    this.components.forEach(callbackfn, thisArg);
  }
  filter(
    predicate: (value: Child, index: number, array: Child[]) => unknown,
    thisArg?: any
  ): any[] {
    return this.components.filter(predicate, thisArg);
  }
  find(
    predicate: (value: Child, index: number, obj: Child[]) => unknown,
    thisArg?: any
  ): Child | undefined {
    return this.components.find(predicate, thisArg);
  }
  clear(): void {
    this.components = [];
  }
}
