import { GroupByClause, GroupByExpression, GroupByExpressionDetails } from './GroupByClause';
import { Column } from './Column';
import { DbField } from '../../DatabaseClient';
import { isString } from 'lodash';

describe('GroupByExpression', () => {
  it('should represent a Column by default', () => {
    const groupByExpression = new GroupByExpression();
    expect(groupByExpression.hasSelectStatementColumn()).toEqual(true);
  });

  it('should deserialize', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const column = new Column('MAX', 'foo', dbField);
    const manualExpression = 'a + b';

    const checkDeserialization = (field?: Column | DbField | string): void => {
      const groupByExpression = new GroupByExpression(field);

      const serializedGroupByExpression = groupByExpression.serialize();
      const expectedSerializedGroupByExpression: GroupByExpressionDetails = {
        selectStatementField: field instanceof Column ? (field as Column)?.serialize() : undefined,
        tableField: field instanceof DbField ? (field as DbField) : undefined,
        manualExpression: isString(field) ? (field as string) || '' : '',
      };

      expect(serializedGroupByExpression).toEqual(expectedSerializedGroupByExpression);

      const deserializedGroupByExpression = GroupByExpression.deserialize(
        serializedGroupByExpression
      );
      expect(deserializedGroupByExpression.equalsTo(groupByExpression)).toEqual(true);
    };

    checkDeserialization(column);
    checkDeserialization(dbField);
    checkDeserialization(manualExpression);
  });

  it('should compare to another GroupByExpression', () => {
    const dbField1 = new DbField('testdb', 't1', 'a', 'int');
    const dbField2 = new DbField('testdb', 't2', 'b', 'int');

    const column1 = new Column('MAX', 'foo', dbField1);
    const column2 = new Column('AVG', 'bar', dbField2);

    const manualExpression1 = 'a + b';
    const manualExpression2 = 'a + c';

    expect(new GroupByExpression(column1).equalsTo(new GroupByExpression(column1))).toEqual(true);
    expect(new GroupByExpression(dbField1).equalsTo(new GroupByExpression(dbField1))).toEqual(true);
    expect(
      new GroupByExpression(manualExpression1).equalsTo(new GroupByExpression(manualExpression1))
    ).toEqual(true);

    expect(new GroupByExpression(column1).equalsTo(new GroupByExpression(column2))).toEqual(false);
    expect(new GroupByExpression(column1).equalsTo(new GroupByExpression(dbField1))).toEqual(false);
    expect(
      new GroupByExpression(column1).equalsTo(new GroupByExpression(manualExpression1))
    ).toEqual(false);

    expect(new GroupByExpression(dbField1).equalsTo(new GroupByExpression(column1))).toEqual(false);
    expect(new GroupByExpression(dbField1).equalsTo(new GroupByExpression(dbField2))).toEqual(
      false
    );
    expect(
      new GroupByExpression(dbField1).equalsTo(new GroupByExpression(manualExpression1))
    ).toEqual(false);

    expect(
      new GroupByExpression(manualExpression1).equalsTo(new GroupByExpression(column1))
    ).toEqual(false);
    expect(
      new GroupByExpression(manualExpression1).equalsTo(new GroupByExpression(dbField1))
    ).toEqual(false);
    expect(
      new GroupByExpression(manualExpression1).equalsTo(new GroupByExpression(manualExpression2))
    ).toEqual(false);
  });

  it('should return proper sql', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const column = new Column('MAX', 'foo', dbField);
    const manualExpression = 'a + b';

    // the GroupByExpression can be represented by: Column, DbField, manual expression;
    // being represented by a Column, GroupByExpression.getSql() should return
    // Column.getSqlWithoutAlias()
    expect(new GroupByExpression(column).getSql()).toEqual(column.getSqlWithoutAlias());
    // being represented by a DbField, GroupByExpression.getSql() should return
    // DbField.getSql()
    expect(new GroupByExpression(dbField).getSql()).toEqual(dbField.getSql());
    // being represented by a manual expression, GroupByExpression.getSql() should return
    // the manual expression itself
    expect(new GroupByExpression(manualExpression).getSql()).toEqual(manualExpression);
  });

  it('should clear other fields on setting one', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const column = new Column('MAX', 'foo', dbField);
    const manualExpression = 'a + b';

    let groupByExpression = new GroupByExpression(column);
    expect(groupByExpression.hasSelectStatementColumn()).toEqual(true);
    expect(groupByExpression.hasTableField()).toEqual(false);
    expect(groupByExpression.manualExpression).toEqual('');

    groupByExpression.tableField = dbField;
    expect(groupByExpression.hasSelectStatementColumn()).toEqual(false);
    expect(groupByExpression.hasTableField()).toEqual(true);
    expect(groupByExpression.manualExpression).toEqual('');

    groupByExpression.manualExpression = manualExpression;
    expect(groupByExpression.hasSelectStatementColumn()).toEqual(false);
    expect(groupByExpression.hasTableField()).toEqual(false);
    expect(groupByExpression.manualExpression).toEqual(manualExpression);
  });
});

describe('GroupByClause', () => {
  let dbField: DbField;
  let column: Column;
  let groupByExpression1: GroupByExpression;
  let groupByExpression2: GroupByExpression;
  let groupByExpression3: GroupByExpression;
  let groupByClause: GroupByClause;

  beforeEach(() => {
    dbField = new DbField('testdb', 't1', 'a', 'int');
    column = new Column('MAX', 'foo', dbField);
    groupByExpression1 = new GroupByExpression(column);
    groupByExpression2 = new GroupByExpression(dbField);
    groupByExpression3 = new GroupByExpression('a + b');

    groupByClause = new GroupByClause();
    groupByClause.add(groupByExpression1);
    groupByClause.add(groupByExpression2);
    groupByClause.add(groupByExpression3);
  });

  it('should deserialize', () => {
    const serializedGroupByClause = groupByClause.serialize();
    const expectedSerializedGroupByClause: GroupByExpressionDetails[] = [
      groupByExpression1.serialize(),
      groupByExpression2.serialize(),
      groupByExpression3.serialize(),
    ];
    expect(serializedGroupByClause).toEqual(expectedSerializedGroupByClause);

    const deserializedGroupByClause = GroupByClause.deserialize(serializedGroupByClause);
    expect(deserializedGroupByClause.equalsTo(groupByClause)).toEqual(true);
  });

  it('should return proper sql', () => {
    const expectedSql = `\t${groupByExpression1.getSql()},\n\t${groupByExpression2.getSql()},\n\t${groupByExpression3.getSql()}`;
    expect(groupByClause.getSql()).toEqual(expectedSql);
  });

  it('should remove column', () => {
    expect(groupByClause.childrenCount()).toEqual(3);
    groupByClause.onSelectStatementColumnRemove(column);
    expect(groupByClause.childrenCount()).toEqual(2);
    groupByClause.forEach((groupByExpression) => {
      expect(groupByExpression.hasSelectStatementColumn()).toEqual(false);
    });
  });

  it('should update column', () => {
    expect(groupByClause.childrenCount()).toEqual(3);
    groupByClause.forEach((groupByExpression) => {
      if (groupByExpression.hasSelectStatementColumn()) {
        expect(groupByExpression.selectStatementColumn!.equalsTo(column)).toEqual(true);
      }
    });

    const newColumn = new Column('MAX', 'foo', undefined, 'a + b');
    groupByClause.onSelectStatementColumnUpdate(column, newColumn);
    expect(groupByClause.childrenCount()).toEqual(3);
    groupByClause.forEach((groupByExpression) => {
      if (groupByExpression.hasSelectStatementColumn()) {
        expect(groupByExpression.selectStatementColumn!.equalsTo(newColumn)).toEqual(true);
      }
    });
  });

  it('should remove on column change to asterisk', () => {
    expect(groupByClause.childrenCount()).toEqual(3);

    const newColumn = new Column(
      column.aggregation,
      column.alias,
      column.dbField,
      column.manualExpression
    );
    newColumn.dbField = DbField.createAsteriskField('testdb', 't1');
    groupByClause.onSelectStatementColumnUpdate(column, newColumn);
    expect(groupByClause.childrenCount()).toEqual(2);
    groupByClause.forEach((groupByExpression) => {
      expect(groupByExpression.hasSelectStatementColumn()).toEqual(false);
    });
  });
});
