import {
  GrafanaVariable,
  WhereClause,
  WhereExpression,
  WhereExpressionArgument,
  WhereExpressionArgumentDetails,
  WhereExpressionDetails,
} from './WhereClause';
import { DbField } from '../../DatabaseClient';
import { BoolOperation } from './BoolOperation';
import { isString } from 'lodash';

describe('WhereExpressionArgument', () => {
  it('should represent a DbField by default', () => {
    const whereExpressionArgument = new WhereExpressionArgument();
    expect(whereExpressionArgument.hasDbField()).toEqual(true);
  });

  it('should compare to another WhereExpressionArgument', () => {
    const whereExpressionArgumentDbField = new WhereExpressionArgument(
      new DbField('testdb', 't1', 'a', 'int')
    );
    const whereExpressionArgumentManualExpression = new WhereExpressionArgument('a + b');
    const whereExpressionArgumentGrafanaVariable = new WhereExpressionArgument(
      new GrafanaVariable('foo')
    );

    expect(
      whereExpressionArgumentDbField.equalsTo(
        new WhereExpressionArgument(new DbField('testdb', 't1', 'a', 'int'))
      )
    ).toEqual(true);
    expect(
      whereExpressionArgumentDbField.equalsTo(
        new WhereExpressionArgument(new DbField('testdb', 't1', 'b', 'int'))
      )
    ).toEqual(false);
    expect(
      whereExpressionArgumentDbField.equalsTo(whereExpressionArgumentManualExpression)
    ).toEqual(false);
    expect(whereExpressionArgumentDbField.equalsTo(whereExpressionArgumentGrafanaVariable)).toEqual(
      false
    );

    expect(
      whereExpressionArgumentManualExpression.equalsTo(new WhereExpressionArgument('a + b'))
    ).toEqual(true);
    expect(
      whereExpressionArgumentManualExpression.equalsTo(new WhereExpressionArgument('a + c'))
    ).toEqual(false);
    expect(
      whereExpressionArgumentManualExpression.equalsTo(whereExpressionArgumentDbField)
    ).toEqual(false);
    expect(
      whereExpressionArgumentManualExpression.equalsTo(whereExpressionArgumentGrafanaVariable)
    ).toEqual(false);

    expect(
      whereExpressionArgumentGrafanaVariable.equalsTo(
        new WhereExpressionArgument(new GrafanaVariable('foo'))
      )
    ).toEqual(true);
    expect(
      whereExpressionArgumentGrafanaVariable.equalsTo(
        new WhereExpressionArgument(new GrafanaVariable('bar'))
      )
    ).toEqual(false);
    expect(whereExpressionArgumentGrafanaVariable.equalsTo(whereExpressionArgumentDbField)).toEqual(
      false
    );
    expect(
      whereExpressionArgumentGrafanaVariable.equalsTo(whereExpressionArgumentManualExpression)
    ).toEqual(false);
  });

  it('should deserialize', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const manualExpression = 'a + b';
    const grafanaVariable = 'foo';

    const checkDeserialization = (field?: DbField | string | GrafanaVariable): void => {
      const whereExpressionArgument = new WhereExpressionArgument(field);
      const serializedWhereExpressionArgument = whereExpressionArgument.serialize();
      const expectedSerializedWhereExpressionArgument: WhereExpressionArgumentDetails = {
        dbField: field instanceof DbField ? (field as DbField) : undefined,
        manualExpression: isString(field) ? (field as string) : undefined,
        grafanaVariable: field instanceof GrafanaVariable ? (field as GrafanaVariable) : undefined,
      };
      expect(serializedWhereExpressionArgument).toEqual(expectedSerializedWhereExpressionArgument);

      const deserializedWhereExpressionArgument = WhereExpressionArgument.deserialize(
        serializedWhereExpressionArgument
      );
      expect(deserializedWhereExpressionArgument.equalsTo(whereExpressionArgument)).toEqual(true);
    };

    checkDeserialization(dbField);
    checkDeserialization(manualExpression);
    checkDeserialization(grafanaVariable);
  });

  it('should return proper sql', () => {
    const dbField = new DbField('testdb', 't1', 'a', 'int');
    const manualExpression = 'a + b';
    const grafanaVariable = 'foo';

    // the WhereExpressionArgument can be represented by:
    // DbField, manual expression, or grafana variable;

    // being represented by a DbField, WhereExpressionArgument.getSql() should return
    // DbField.getSql()
    expect(new WhereExpressionArgument(dbField).getSql()).toEqual(dbField.getSql());
    // being represented by a manual expression, WhereExpressionArgument.getSql() should return
    // that manual expression
    expect(new WhereExpressionArgument(manualExpression).getSql()).toEqual(manualExpression);
    // being represented by a grafana variable, WhereExpressionArgument.getSql() should return
    // that grafana variable with the dollar-sign
    expect(new WhereExpressionArgument(new GrafanaVariable(grafanaVariable)).getSql()).toEqual(
      `$${grafanaVariable}`
    );
  });

  it('should clear other fields on setting one', () => {
    let whereExpressionArgument = new WhereExpressionArgument(new DbField());
    expect(whereExpressionArgument.hasDbField()).toEqual(true);
    expect(whereExpressionArgument.dbField).toEqual(new DbField());
    expect(whereExpressionArgument.hasManualExpression()).toEqual(false);
    expect(whereExpressionArgument.manualExpression).toBeUndefined();
    expect(whereExpressionArgument.grafanaVariable).toBeUndefined();

    whereExpressionArgument.manualExpression = 'a + b';
    expect(whereExpressionArgument.hasDbField()).toEqual(false);
    expect(whereExpressionArgument.dbField).toBeUndefined();
    expect(whereExpressionArgument.hasManualExpression()).toEqual(true);
    expect(whereExpressionArgument.grafanaVariable).toBeUndefined();

    whereExpressionArgument.grafanaVariable = new GrafanaVariable('foo');
    expect(whereExpressionArgument.hasDbField()).toEqual(false);
    expect(whereExpressionArgument.dbField).toBeUndefined();
    expect(whereExpressionArgument.hasManualExpression()).toEqual(false);
    expect(whereExpressionArgument.manualExpression).toBeUndefined();
    expect(whereExpressionArgument.grafanaVariable).toEqual(new GrafanaVariable('foo'));

    whereExpressionArgument.dbField = new DbField();
    expect(whereExpressionArgument.hasDbField()).toEqual(true);
    expect(whereExpressionArgument.dbField).toEqual(new DbField());
    expect(whereExpressionArgument.hasManualExpression()).toEqual(false);
    expect(whereExpressionArgument.manualExpression).toBeUndefined();
    expect(whereExpressionArgument.grafanaVariable).toBeUndefined();
  });
});

describe('WhereExpression', () => {
  it('should compare to another WhereExpression', () => {
    const dbField1 = new DbField('testdb', 't1', 'a', 'int');
    const dbField2 = new DbField('testdb', 't1', 'b', 'int');
    const manualExpression1 = 'a + b';
    const manualExpression2 = 'a + c';
    const grafanaVariable1 = 'foo';
    const grafanaVariable2 = 'bar';

    const argumentDbField1 = new WhereExpressionArgument(dbField1);
    const argumentDbField2 = new WhereExpressionArgument(dbField2);
    const argumentManualExpression1 = new WhereExpressionArgument(manualExpression1);
    const argumentManualExpression2 = new WhereExpressionArgument(manualExpression2);
    const argumentGrafanaVariable1 = new WhereExpressionArgument(grafanaVariable1);
    const argumentGrafanaVariable2 = new WhereExpressionArgument(grafanaVariable2);

    expect(new WhereExpression().equalsTo(new WhereExpression())).toEqual(true);

    expect(
      new WhereExpression(argumentDbField1, argumentDbField1, BoolOperation.Equal).equalsTo(
        new WhereExpression(argumentDbField1, argumentDbField1, BoolOperation.Equal)
      )
    ).toEqual(true);
    expect(
      new WhereExpression(argumentDbField1, argumentDbField1, BoolOperation.Equal).equalsTo(
        new WhereExpression(argumentDbField1, argumentDbField2, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new WhereExpression(argumentDbField1, argumentDbField1, BoolOperation.Equal).equalsTo(
        new WhereExpression(argumentDbField2, argumentDbField1, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new WhereExpression(argumentDbField1, argumentDbField1, BoolOperation.Equal).equalsTo(
        new WhereExpression(argumentDbField1, argumentManualExpression1, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new WhereExpression(argumentDbField1, argumentDbField1, BoolOperation.Equal).equalsTo(
        new WhereExpression(argumentGrafanaVariable1, argumentDbField1, BoolOperation.Equal)
      )
    ).toEqual(false);

    expect(
      new WhereExpression(
        argumentManualExpression1,
        argumentManualExpression1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(
          argumentManualExpression1,
          argumentManualExpression1,
          BoolOperation.Equal
        )
      )
    ).toEqual(true);
    expect(
      new WhereExpression(
        argumentManualExpression1,
        argumentManualExpression1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(
          argumentManualExpression1,
          argumentManualExpression2,
          BoolOperation.Equal
        )
      )
    ).toEqual(false);
    expect(
      new WhereExpression(
        argumentManualExpression1,
        argumentManualExpression1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(
          argumentManualExpression2,
          argumentManualExpression1,
          BoolOperation.Equal
        )
      )
    ).toEqual(false);
    expect(
      new WhereExpression(
        argumentManualExpression1,
        argumentManualExpression1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(argumentManualExpression1, argumentDbField1, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new WhereExpression(
        argumentManualExpression1,
        argumentManualExpression1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(
          argumentGrafanaVariable1,
          argumentManualExpression1,
          BoolOperation.Equal
        )
      )
    ).toEqual(false);

    expect(
      new WhereExpression(
        argumentGrafanaVariable1,
        argumentGrafanaVariable1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(argumentGrafanaVariable1, argumentGrafanaVariable1, BoolOperation.Equal)
      )
    ).toEqual(true);
    expect(
      new WhereExpression(
        argumentGrafanaVariable1,
        argumentGrafanaVariable1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(argumentGrafanaVariable1, argumentGrafanaVariable2, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new WhereExpression(
        argumentGrafanaVariable1,
        argumentGrafanaVariable1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(argumentGrafanaVariable2, argumentGrafanaVariable1, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new WhereExpression(
        argumentGrafanaVariable1,
        argumentGrafanaVariable1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(argumentGrafanaVariable1, argumentDbField1, BoolOperation.Equal)
      )
    ).toEqual(false);
    expect(
      new WhereExpression(
        argumentGrafanaVariable1,
        argumentGrafanaVariable1,
        BoolOperation.Equal
      ).equalsTo(
        new WhereExpression(
          argumentManualExpression1,
          argumentGrafanaVariable1,
          BoolOperation.Equal
        )
      )
    ).toEqual(false);
  });

  it('should deserialize', () => {
    const argumentDbField = new WhereExpressionArgument(new DbField('testdb', 't1', 'a', 'int'));
    const argumentManualExpression = new WhereExpressionArgument('a + b');
    const argumentGrafanaVariable = new WhereExpressionArgument(new GrafanaVariable('foo'));

    const checkDeserialization = (
      left?: WhereExpressionArgument,
      right?: WhereExpressionArgument,
      op?: BoolOperation
    ): void => {
      const whereExpression = new WhereExpression(left, right, op);
      const serializedWhereExpression = whereExpression.serialize();
      const expectedSerializedWhereExpression: WhereExpressionDetails = {
        leftExpression: left?.serialize(),
        rightExpression: right?.serialize(),
        operation: op,
      };
      expect(serializedWhereExpression).toEqual(expectedSerializedWhereExpression);

      const deserializedWhereExpression = WhereExpression.deserialize(serializedWhereExpression);
      expect(deserializedWhereExpression.equalsTo(whereExpression)).toEqual(true);
    };

    checkDeserialization();
    checkDeserialization(argumentDbField, argumentDbField, BoolOperation.Equal);
    checkDeserialization(argumentDbField, argumentManualExpression, BoolOperation.LessOrEqual);
    checkDeserialization(
      argumentGrafanaVariable,
      argumentManualExpression,
      BoolOperation.GreaterOrEqual
    );
    checkDeserialization(argumentGrafanaVariable, argumentDbField, BoolOperation.LessThan);
  });

  it('should return proper sql', () => {
    expect(new WhereExpression().getSql()).toEqual('');

    const argumentDbField = new WhereExpressionArgument(new DbField('testdb', 't1', 'a', 'int'));
    const argumentManualExpression = new WhereExpressionArgument('a + b');
    const argumentGrafanaVariable = new WhereExpressionArgument(new GrafanaVariable('foo'));

    const whereExpression1 = new WhereExpression(
      argumentDbField,
      argumentManualExpression,
      BoolOperation.Equal
    );
    const expectedSql1 = `${argumentDbField.getSql()} = ${argumentManualExpression.getSql()}`;
    expect(whereExpression1.getSql()).toEqual(expectedSql1);

    const whereExpression2 = new WhereExpression(
      argumentDbField,
      argumentGrafanaVariable,
      BoolOperation.Equal
    );
    const expectedSql2 = `${argumentDbField.getSql()} = ${argumentGrafanaVariable.getSql()}`;
    expect(whereExpression2.getSql()).toEqual(expectedSql2);

    const whereExpression3 = new WhereExpression(
      argumentGrafanaVariable,
      argumentManualExpression,
      BoolOperation.Equal
    );
    const expectedSql3 = `${argumentGrafanaVariable.getSql()} = ${argumentManualExpression.getSql()}`;
    expect(whereExpression3.getSql()).toEqual(expectedSql3);
  });
});

describe('WhereClause', () => {
  it('should combine expressions by AND', () => {
    let whereClause = new WhereClause();

    const whereExpression1 = new WhereExpression(
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'a')),
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'b')),
      BoolOperation.GreaterOrEqual
    );

    const whereExpression2 = new WhereExpression(
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'c')),
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'd')),
      BoolOperation.GreaterOrEqual
    );

    whereClause.add(whereExpression1);
    whereClause.add(whereExpression2);

    expect(whereClause.getSql()).toEqual(
      `\t${whereExpression1.getSql()} AND \n\t${whereExpression2.getSql()}`
    );
  });

  it('should deserialize', () => {
    let whereClause = new WhereClause();

    const whereExpression1 = new WhereExpression(
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'a')),
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'b')),
      BoolOperation.GreaterOrEqual
    );

    const whereExpression2 = new WhereExpression(
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'c')),
      new WhereExpressionArgument(new DbField('testdb', 'tbl', 'd')),
      BoolOperation.GreaterOrEqual
    );

    whereClause.add(whereExpression1);
    whereClause.add(whereExpression2);

    const serializedWhereClause = whereClause.serialize();
    const expectedSerializedWhereClause: WhereExpressionDetails[] = [
      whereExpression1.serialize(),
      whereExpression2.serialize(),
    ];
    expect(serializedWhereClause).toEqual(expectedSerializedWhereClause);

    const deserializedWhereClause = WhereClause.deserialize(serializedWhereClause);
    expect(deserializedWhereClause.equalsTo(whereClause)).toEqual(true);
  });
});
