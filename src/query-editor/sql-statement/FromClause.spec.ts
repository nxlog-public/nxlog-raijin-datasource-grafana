import { FromClause, FromClauseDetails } from './FromClause';

describe('FromClause', () => {
  it('should default construct an empty object', () => {
    const fromClause = new FromClause();
    expect(fromClause.tableName).toEqual('');
    expect(fromClause.databaseName).toEqual('');
    expect(fromClause.getSql()).toEqual('');
  });

  it('should clear table name on database change', () => {
    const fromClause = new FromClause('testdb', 't1');

    expect(fromClause.tableName).not.toEqual('');
    fromClause.databaseName = 'testdb1';
    expect(fromClause.tableName).toEqual('');
  });

  it('should deserialize', () => {
    const fromClause = new FromClause('testdb', 't1');
    const serializedFromClause = fromClause.serialize();

    const expectedSerializedFromClause: FromClauseDetails = {
      databaseName: fromClause.databaseName,
      tableName: fromClause.tableName,
    };

    expect(serializedFromClause).toEqual(expectedSerializedFromClause);
    expect(FromClause.deserialize(serializedFromClause).equalsTo(fromClause)).toEqual(true);
  });

  it('should compare to another FromClause', () => {
    const fromClause = new FromClause('testdb', 't1');

    expect(fromClause.equalsTo(new FromClause('testdb', 't1'))).toEqual(true);

    expect(fromClause.equalsTo(new FromClause())).toEqual(false);
    expect(fromClause.equalsTo(new FromClause('testdb', 't2'))).toEqual(false);
    expect(fromClause.equalsTo(new FromClause('testdb1', 't1'))).toEqual(false);
  });
});
