import { DateTime, dateTime, TimeRange } from '@grafana/data';
import { DbField } from 'DatabaseClient';
import { BoolOperation } from './BoolOperation';
import { TimeColumn, TimeInterval, TimeIntervalPeriod } from './TimeColumn';
import { Column, ColumnList } from './Column';
import { SelectStatement, SelectStatementDetails } from './SelectStatement';
import { FromClause } from './FromClause';
import { WhereClause, WhereExpression, WhereExpressionArgument } from './WhereClause';
import { GroupByClause, GroupByExpression } from './GroupByClause';
import { HavingClause, HavingExpression, HavingExpressionArgument } from './HavingClause';

describe('SelectStatement', () => {
  it('should synchronize group by with select columns', () => {
    const select = new SelectStatement(new FromClause('foo', 'bar'));
    select.timeColumn.enabled = false;
    select.selectColumns.add(
      new Column('AVG', 'barbar', new DbField('foo', 'bar', 'column1', 'integer'))
    );
    select.selectColumns.add(
      new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'))
    );

    const expectedGroupByClause = new GroupByClause();
    expectedGroupByClause.add(
      new GroupByExpression(
        new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'), '')
      )
    );

    expect(select.groupByClause.equalsTo(expectedGroupByClause)).toEqual(true);
  });

  it('should clear group by on updating column to be a non-aggregation', () => {
    const select = new SelectStatement(new FromClause('foo', 'bar'));
    select.timeColumn.enabled = false;

    let aggColumn = new Column('AVG', 'barbar', new DbField('foo', 'bar', 'column1', 'integer'));
    select.selectColumns.add(aggColumn);
    select.selectColumns.add(
      new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'))
    );

    aggColumn.aggregation = undefined;
    select.selectColumns.update(0, aggColumn); // this will trigger an event

    expect(select.groupByClause.childrenCount()).toEqual(0);
  });

  it('should clear group by on aggregation deletetion', () => {
    const select = new SelectStatement(new FromClause('foo', 'bar'));
    select.timeColumn.enabled = false;

    select.selectColumns.add(
      new Column('AVG', 'barbar', new DbField('foo', 'bar', 'column1', 'integer'))
    );
    select.selectColumns.add(
      new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'))
    );

    select.selectColumns.remove(0); // this will trigger an event
    expect(select.groupByClause.childrenCount()).toEqual(0);
  });

  it('should add non-agg columns after updating existing column to be an aggregation', () => {
    const select = new SelectStatement(new FromClause('foo', 'bar'));
    select.timeColumn.enabled = false;

    let aggColumn = new Column(
      undefined,
      'barbar',
      new DbField('foo', 'bar', 'column1', 'integer')
    );
    select.selectColumns.add(aggColumn);
    select.selectColumns.add(
      new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'))
    );
    select.selectColumns.add(
      new Column(undefined, 'foofoo', new DbField('foo', 'bar', 'column3', 'string'))
    );

    expect(select.groupByClause.childrenCount()).toEqual(0);

    aggColumn.aggregation = 'COUNT';
    select.selectColumns.update(0, aggColumn); // this will trigger an event

    const expectedGroupByClause = new GroupByClause();
    expectedGroupByClause.add(
      new GroupByExpression(
        new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'), '')
      )
    );
    expectedGroupByClause.add(
      new GroupByExpression(
        new Column(undefined, 'foofoo', new DbField('foo', 'bar', 'column3', 'string'), '')
      )
    );
    expect(select.groupByClause.equalsTo(expectedGroupByClause)).toEqual(true);
  });

  it('should syncronize time column', () => {
    const timeField = new DbField('foo', 'bar', 'tm', 'datetime');
    const select = new SelectStatement(new FromClause('foo', 'bar'));
    select.timeColumn.enabled = true;
    select.timeColumn.field = timeField;

    select.selectColumns.add(
      new Column('AVG', 'barbar', new DbField('foo', 'bar', 'column1', 'integer'))
    );
    select.selectColumns.add(
      new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'))
    );

    const expectedGroupByClause = new GroupByClause();
    expectedGroupByClause.add(new GroupByExpression(timeField));
    expectedGroupByClause.add(
      new GroupByExpression(
        new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'), '')
      )
    );

    expect(select.groupByClause.equalsTo(expectedGroupByClause)).toEqual(true);
  });

  it('should deserialize to correct state', () => {
    const timeField = new DbField('foo', 'bar', 'tm', 'datetime');
    const select = new SelectStatement(new FromClause('foo', 'bar'));
    select.timeColumn.enabled = true;
    select.timeColumn.field = timeField;

    select.selectColumns.add(
      new Column('AVG', 'barbar', new DbField('foo', 'bar', 'column1', 'integer'))
    );
    select.selectColumns.add(
      new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'))
    );

    const expectedGroupByClause = new GroupByClause();
    expectedGroupByClause.add(new GroupByExpression(timeField));
    expectedGroupByClause.add(
      new GroupByExpression(
        new Column(undefined, 'barbar', new DbField('foo', 'bar', 'column2', 'integer'), '')
      )
    );

    const serializedSelect = select.serialize();
    const deserializedSelect = SelectStatement.deserialize(serializedSelect);
    expect(deserializedSelect.equalsTo(select)).toEqual(true);
    expect(deserializedSelect.groupByClause.equalsTo(expectedGroupByClause)).toEqual(true);

    deserializedSelect.timeColumn.enabled = false;
    expect(deserializedSelect.groupByClause.equalsTo(expectedGroupByClause)).toEqual(false);

    expectedGroupByClause.remove(0);
    expect(deserializedSelect.groupByClause.equalsTo(expectedGroupByClause)).toEqual(true);

    deserializedSelect.selectColumns.remove(0); // remove aggregation
    expect(deserializedSelect.groupByClause.childrenCount()).toEqual(0);
  });

  it('should deserialize undefined', () => {
    const details = {};

    expect(() =>
      SelectStatement.deserialize(details as unknown as SelectStatementDetails)
    ).not.toThrow();

    const deserializedSelectStatement = SelectStatement.deserialize(
      details as unknown as SelectStatementDetails
    );

    expect(deserializedSelectStatement.fromClause.equalsTo(new FromClause())).toEqual(true);
    expect(deserializedSelectStatement.selectColumns.equalsTo(new ColumnList())).toEqual(true);
    expect(deserializedSelectStatement.whereClause.equalsTo(new WhereClause())).toEqual(true);
    expect(deserializedSelectStatement.groupByClause.equalsTo(new GroupByClause())).toEqual(true);
    expect(deserializedSelectStatement.havingClause.equalsTo(new HavingClause())).toEqual(true);
    expect(deserializedSelectStatement.timeColumn.equalsTo(new TimeColumn())).toEqual(true);
  });

  it('should disable event processing on tearDown', () => {
    const disableEventProcessingMock = jest.spyOn(
      SelectStatement.prototype as any,
      'disableEventProcessing'
    );
    disableEventProcessingMock.mockImplementation(() => {});

    const selectStatement = new SelectStatement();
    selectStatement.tearDown();
    expect(disableEventProcessingMock).toBeCalled();

    disableEventProcessingMock.mockRestore();
  });

  it('should reset', () => {
    let selectSstatement = new SelectStatement();
    selectSstatement.syncronizeColumns = false;
    selectSstatement.fromClause = new FromClause('testdb', 'tbl');
    selectSstatement.selectColumns.add(new Column());
    selectSstatement.whereClause.add(new WhereExpression());
    selectSstatement.groupByClause.add(new GroupByExpression());
    selectSstatement.havingClause.add(new HavingExpression());
    selectSstatement.timeColumn = new TimeColumn(
      true,
      false,
      new DbField('testdb', 'tbl', 'a', 'int'),
      true,
      new TimeInterval(1, TimeIntervalPeriod.HOUR)
    );

    expect(selectSstatement.fromClause.equalsTo(new FromClause())).toEqual(false);
    expect(selectSstatement.selectColumns.childrenCount()).toEqual(1);
    expect(selectSstatement.whereClause.childrenCount()).toEqual(1);
    expect(selectSstatement.groupByClause.childrenCount()).toEqual(1);
    expect(selectSstatement.havingClause.childrenCount()).toEqual(1);
    expect(selectSstatement.timeColumn.enabled).toEqual(true);
    expect(
      selectSstatement.timeColumn.field?.equalsTo(new DbField('testdb', 'tbl', 'a', 'int'))
    ).toEqual(true);
    expect(selectSstatement.timeColumn.groupByTimeInterval).toEqual(true);
    expect(selectSstatement.timeColumn.timeInterval).toEqual(
      new TimeInterval(1, TimeIntervalPeriod.HOUR)
    );

    selectSstatement.reset();

    expect(selectSstatement.fromClause.databaseName).toEqual('');
    expect(selectSstatement.fromClause.tableName).toEqual('');
    expect(selectSstatement.selectColumns.childrenCount()).toEqual(0);
    expect(selectSstatement.whereClause.childrenCount()).toEqual(0);
    expect(selectSstatement.groupByClause.childrenCount()).toEqual(0);
    expect(selectSstatement.havingClause.childrenCount()).toEqual(0);
    expect(selectSstatement.timeColumn.enabled).toEqual(true);
    expect(selectSstatement.timeColumn.field).toBeUndefined();
    expect(selectSstatement.timeColumn.groupByTimeInterval).toEqual(false);
    expect(selectSstatement.timeColumn.timeInterval).toBeUndefined();
  });

  it('should return proper value for hasChildren', () => {
    let selectStatement = new SelectStatement();

    expect(selectStatement.hasChildren()).toEqual(false);

    selectStatement.selectColumns.add(new Column());
    expect(selectStatement.hasChildren()).toEqual(true);

    selectStatement.reset();
    expect(selectStatement.hasChildren()).toEqual(false);
    selectStatement.whereClause.add(new WhereExpression());
    expect(selectStatement.hasChildren()).toEqual(true);

    selectStatement.reset();
    expect(selectStatement.hasChildren()).toEqual(false);
    selectStatement.groupByClause.add(new GroupByExpression());
    expect(selectStatement.hasChildren()).toEqual(true);

    selectStatement.reset();
    expect(selectStatement.hasChildren()).toEqual(false);
    selectStatement.havingClause.add(new HavingExpression());
    expect(selectStatement.hasChildren()).toEqual(true);
  });

  it('should turn synchronization on and off', () => {
    const enableEventProcessingMock = jest.spyOn(
      SelectStatement.prototype as any,
      'enableEventProcessing'
    );
    enableEventProcessingMock.mockImplementation(() => {});
    const disableEventProcessingMock = jest.spyOn(
      SelectStatement.prototype as any,
      'disableEventProcessing'
    );
    disableEventProcessingMock.mockImplementation(() => {});

    let selectStatement = new SelectStatement();

    // by default, synchronization is on
    expect(selectStatement.syncronizeColumns).toEqual(true);

    enableEventProcessingMock.mockClear();
    disableEventProcessingMock.mockClear();

    selectStatement.syncronizeColumns = false;
    expect(disableEventProcessingMock).toBeCalled();
    expect(enableEventProcessingMock).not.toBeCalled();

    enableEventProcessingMock.mockClear();
    disableEventProcessingMock.mockClear();

    selectStatement.syncronizeColumns = true;
    expect(disableEventProcessingMock).not.toBeCalled();
    expect(enableEventProcessingMock).toBeCalled();

    enableEventProcessingMock.mockRestore();
    disableEventProcessingMock.mockRestore();
  });

  it('should return proper sql', () => {
    const timeColumn = new TimeColumn(false, false); // disabled time column

    const dbField = new DbField('testdb', 't1', 'a', 'int');

    const countColumn = new Column('COUNT', '', DbField.createAsteriskField('testdb', 't1'));
    let columnList = new ColumnList();
    columnList.add(countColumn);
    columnList.add(new Column(undefined, 'foo', dbField));

    const whereClause = new WhereClause();
    whereClause.add(
      new WhereExpression(
        new WhereExpressionArgument(dbField),
        new WhereExpressionArgument('0'),
        BoolOperation.GreaterThan
      )
    );

    const groupByClause = new GroupByClause();
    groupByClause.add(new GroupByExpression(new DbField('testdb', 't1', 'b', 'int')));

    const havingClause = new HavingClause();
    havingClause.add(
      new HavingExpression(
        new HavingExpressionArgument(countColumn),
        new HavingExpressionArgument('10'),
        BoolOperation.GreaterThan
      )
    );

    const selectStatement = new SelectStatement(
      new FromClause('testdb', 't1'),
      columnList,
      whereClause,
      groupByClause,
      havingClause,
      timeColumn
    );

    const startTime: DateTime = dateTime();
    const endTime: DateTime = dateTime().add(10, 'hour');

    const range: TimeRange = {
      from: startTime,
      to: endTime,
      raw: {
        from: startTime,
        to: endTime,
      },
    };

    // 1. TimeColumn is disabled
    let expectedSql =
      'SELECT\n' +
      '\tCOUNT("t1"."*"),\n' +
      '\t"t1"."a" AS "foo"\n' +
      'FROM\n' +
      '\t"testdb"."t1"\n' +
      'WHERE\n' +
      '\t"t1"."a" > 0\n' +
      'GROUP BY\n' +
      '\t"t1"."b",\n' +
      '\t"t1"."a"\n' + // t1.a is added automatically because of aggregation presence
      'HAVING\n' +
      '\tCOUNT("t1"."*") > 10';

    expect(selectStatement.getSql()).toEqual(expectedSql);
    // time column is off, range shouldn't affect on the output
    expect(selectStatement.getSql(range)).toEqual(expectedSql);

    // 2. TimeColumn is enabled
    timeColumn.enabled = true;
    timeColumn.field = new DbField('testdb', 't1', 'dt', 'datetime');
    // should synchronize group by
    expectedSql =
      'SELECT\n' +
      '\t"t1"."dt" AS time,\n' +
      '\tCOUNT("t1"."*"),\n' +
      '\t"t1"."a" AS "foo"\n' +
      'FROM\n' +
      '\t"testdb"."t1"\n' +
      'WHERE\n' +
      '\t"t1"."a" > 0\n' +
      'GROUP BY\n' +
      '\t"t1"."b",\n' +
      '\t"t1"."dt",\n' + // time (t1.dt) is added automatically because of aggregation presence
      '\t"t1"."a"\n' + // t1.a is added automatically because of aggregation presence
      'HAVING\n' +
      '\tCOUNT("t1"."*") > 10';
    expect(selectStatement.getSql()).toEqual(expectedSql);
    // should add filter by time range
    expectedSql =
      'SELECT\n' +
      '\t"t1"."dt" AS time,\n' +
      '\tCOUNT("t1"."*"),\n' +
      '\t"t1"."a" AS "foo"\n' +
      'FROM\n' +
      '\t"testdb"."t1"\n' +
      'WHERE\n' +
      `\t${timeColumn.field.getSql()} >= '${startTime.toISOString()}' AND ${timeColumn.field.getSql()} <= '${endTime.toISOString()}' AND\n` + // this is for the time range filter
      '\t"t1"."a" > 0\n' +
      'GROUP BY\n' +
      '\t"t1"."b",\n' +
      '\t"t1"."dt",\n' + // time (t1.dt) is added automatically because of aggregation presence
      '\t"t1"."a"\n' + // t1.a is added automatically because of aggregation presence
      'HAVING\n' +
      '\tCOUNT("t1"."*") > 10';
    expect(selectStatement.getSql(range)).toEqual(expectedSql);

    // 3. TimeColumn is enabled and grouped by time interval
    timeColumn.groupByTimeInterval = true;
    timeColumn.timeInterval = new TimeInterval(1, TimeIntervalPeriod.HOUR);
    // should calculate time intervals
    expectedSql =
      'SELECT\n' +
      '\tusec_to_timestamp( timestamp_to_usec( "t1"."dt" ) - (timestamp_to_usec( "t1"."dt" ) % 3600000000) ) AS time,\n' +
      '\tCOUNT("t1"."*"),\n' +
      '\t"t1"."a" AS "foo"\n' +
      'FROM\n' +
      '\t"testdb"."t1"\n' +
      'WHERE\n' +
      '\t"t1"."a" > 0\n' +
      'GROUP BY\n' +
      '\ttime,\n' +
      '\t"t1"."b",\n' +
      '\t"t1"."a"\n' +
      'HAVING\n' +
      '\tCOUNT("t1"."*") > 10';
    expect(selectStatement.getSql()).toEqual(expectedSql);
    expectedSql =
      'SELECT\n' +
      '\tusec_to_timestamp( timestamp_to_usec( "t1"."dt" ) - (timestamp_to_usec( "t1"."dt" ) % 3600000000) ) AS time,\n' +
      '\tCOUNT("t1"."*"),\n' +
      '\t"t1"."a" AS "foo"\n' +
      'FROM\n' +
      '\t"testdb"."t1"\n' +
      'WHERE\n' +
      `\t${timeColumn.field.getSql()} >= '${startTime.toISOString()}' AND ${timeColumn.field.getSql()} <= '${endTime.toISOString()}' AND\n` + // this is for the time range filter
      '\t"t1"."a" > 0\n' +
      'GROUP BY\n' +
      '\ttime,\n' +
      '\t"t1"."b",\n' +
      '\t"t1"."a"\n' +
      'HAVING\n' +
      '\tCOUNT("t1"."*") > 10';
    expect(selectStatement.getSql(range)).toEqual(expectedSql);

    // 4. TimeColumn is enabled and autogenerated
    timeColumn.autogenerated = true;
    expectedSql =
      'SELECT\n' +
      '\tCOUNT("t1"."*"),\n' +
      '\t"t1"."a" AS "foo"\n' +
      'FROM\n' +
      '\t"testdb"."t1"\n' +
      'WHERE\n' +
      '\t"t1"."a" > 0\n' +
      'GROUP BY\n' +
      '\t"t1"."b",\n' +
      '\t"t1"."a"\n' +
      'HAVING\n' +
      '\tCOUNT("t1"."*") > 10';
    expect(selectStatement.getSql(range)).toEqual(expectedSql);

    // 5. There is no aggregation
    columnList.remove(0);
    expectedSql =
      'SELECT\n' +
      '\t"t1"."a" AS "foo"\n' +
      'FROM\n' +
      '\t"testdb"."t1"\n' +
      'WHERE\n' +
      '\t"t1"."a" > 0\n' +
      'GROUP BY\n' +
      '\t"t1"."b"'; // HAVING was autoremoved
    expect(selectStatement.getSql(range)).toEqual(expectedSql);

    // 6. Default constructed SelectStatement
    expectedSql =
      `SELECT\n` +
      `\n` +
      `FROM\n` +
      `\n` +
      `WHERE\n` +
      `\tundefined >= '${startTime.toISOString()}' AND undefined <= '${endTime.toISOString()}'`;
    expect(new SelectStatement().getSql(range)).toEqual(expectedSql);
  });
});
