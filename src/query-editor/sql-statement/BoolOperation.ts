export enum BoolOperation {
  Equal = '=',
  LessThan = '<',
  GreaterThan = '>',
  LessOrEqual = '<=',
  GreaterOrEqual = '>=',
  NotEqual = '<>',
  In = 'IN',
  NotIn = 'NOT IN',
}

type ArgumentFormatter = (value: string) => string;
const DefaultFormatter = (value: string) => value;

const BoolOperationArgumentFormatterMap: Map<BoolOperation, ArgumentFormatter> = new Map([
  [BoolOperation.Equal, DefaultFormatter],
  [BoolOperation.LessThan, DefaultFormatter],
  [BoolOperation.GreaterThan, DefaultFormatter],
  [BoolOperation.LessOrEqual, DefaultFormatter],
  [BoolOperation.GreaterOrEqual, DefaultFormatter],
  [BoolOperation.NotEqual, DefaultFormatter],
  [BoolOperation.In, (value: string) => `(${value})`],
  [BoolOperation.NotIn, (value: string) => `(${value})`],
]);

export function FormatBoolOperationArgument(operation: BoolOperation, argument?: string): string {
  const formatter = BoolOperationArgumentFormatterMap.get(operation) || DefaultFormatter;
  return formatter(argument || '');
}
