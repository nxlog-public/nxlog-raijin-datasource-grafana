import { SqlStatementComponent } from './SqlStatement';

/**
 * Describes the FromClause class's data representation exposed to Grafana
 *
 * (for details see {@link FromClause#serialize}, {@link FromClause#deserialize})
 */
export interface FromClauseDetails {
  databaseName: string;
  tableName: string;
}

export class FromClause extends SqlStatementComponent<FromClauseDetails> {
  constructor(private internalDatabaseName = '', public tableName = '') {
    super();
  }

  override serialize(): FromClauseDetails {
    return {
      databaseName: this.internalDatabaseName,
      tableName: this.tableName,
    };
  }

  static deserialize(value: FromClauseDetails): FromClause {
    return new FromClause(value.databaseName, value.tableName);
  }

  override equalsTo(other: FromClause): boolean {
    return (
      this.internalDatabaseName === other.internalDatabaseName && this.tableName === other.tableName
    );
  }

  override getSql(): string {
    if (this.internalDatabaseName === '' || this.tableName === '') {
      return '';
    }
    return `\t"${this.internalDatabaseName}"."${this.tableName}"`;
  }

  get databaseName(): string {
    return this.internalDatabaseName;
  }
  set databaseName(value: string) {
    if (this.internalDatabaseName !== value) {
      this.tableName = '';
    }
    this.internalDatabaseName = value;
  }
}
