import React from 'react';
import { useStyles2 } from '@grafana/ui';
import { GrafanaTheme2 } from '@grafana/data';
import { css } from '@emotion/css';

interface QueryBuilderRowProps {
  children: React.ReactNode;
  height?: string;
  width?: string;
}

const getQueryBuilderRowStyles = (theme: GrafanaTheme2) => {
  return {
    root: css({
      display: 'grid',
      padding: theme.spacing(1, 1, 1, 1),
      marginBottom: 10,
      marginTop: 5,
      backgroundColor: theme.colors.background.secondary,
      borderRadius: theme.shape.borderRadius(1),
    }),
  };
};

export function QueryBuilderRow(props: QueryBuilderRowProps) {
  const styles = useStyles2(getQueryBuilderRowStyles);

  return (
    <div
      className={styles.root}
      style={{
        height: props.height || '100%',
        width: props.width || '100%',
      }}
    >
      {props.children}
    </div>
  );
}
