export type Newable<T> = { new (...args: any[]): T };

export class JsonChecker {
  hasKey(obj: any, searchKey: string): boolean {
    if (!(obj instanceof Object)) {
      return false;
    }
    for (let [key, value] of Object.entries(obj)) {
      if (key === searchKey) {
        return true;
      }
      if (value instanceof Object) {
        return this.hasKey(value, searchKey);
      }
    }
    return false;
  }
}

/**
 * The last query execution error string message
 * The value is, in fact, set by SetLastQueryError function within DataSource::query method
 */
let lastQueryError: string;

export function SetLastQueryError(error: string) {
  lastQueryError = error;
}

export function GetLastQueryError(): string {
  return lastQueryError === undefined ? '' : lastQueryError;
}
