import { GetLastQueryError, JsonChecker, SetLastQueryError } from './common';

describe('JsonChecker', () => {
  it('should find the key', () => {
    const obj = {
      a: 'test',
    };

    const jsonChecker = new JsonChecker();
    expect(jsonChecker.hasKey(obj, 'a')).toEqual(true);
  });

  it('should not find the absent key', () => {
    const obj = {
      a: 'test',
    };

    const jsonChecker = new JsonChecker();
    expect(jsonChecker.hasKey(obj, 'b')).toEqual(false);
  });

  it('should find the key in the nested object', () => {
    const obj = {
      a: 'test',
      b: {
        c: {
          d: 0,
        },
      },
    };

    const jsonChecker = new JsonChecker();
    expect(jsonChecker.hasKey(obj, 'd')).toEqual(true);
  });
});

describe('lastQueryError', () => {
  it('should return empty string', () => {
    expect(GetLastQueryError()).toEqual('');
  });

  it('should properly save the error text', () => {
    const expectedErrorText = 'test error text';
    SetLastQueryError(expectedErrorText);
    expect(GetLastQueryError()).toEqual(expectedErrorText);
  });
});
