import { DataSource } from './datasource';
import {
  CoreApp,
  DataQueryRequest,
  dateTime,
  FieldType,
  MetricFindValue,
  MutableDataFrame,
  MutableVector,
  ScopedVars,
} from '@grafana/data';
import { MyQuery } from './types';
import { BackendSrvRequest } from '@grafana/runtime/services/backendSrv';
import { of } from 'rxjs';
import { GetLastQueryError } from './common';
import {
  Column,
  ColumnList,
  FromClause,
  SelectStatement,
  TimeColumn,
} from './query-editor/sql-statement';
import { TimeRange } from '@grafana/data/types/time';
import { MutableField } from '@grafana/data/dataframe/MutableDataFrame';
import { DbField } from './DatabaseClient';
import {
  createDatasourceSettings,
  createRaijinServerResponse,
  doMockExpectedResponse,
  fetchMock,
  GRAFANA_API_URL,
  RAIJIN_URL,
} from './testcommon';

const templateReplace = (
  target?: string,
  _scopedVars?: ScopedVars,
  _format?: string | Function
): string => {
  return target || '';
};

jest.mock('@grafana/runtime', () => ({
  ...jest.requireActual('@grafana/runtime'),
  getBackendSrv: () => ({
    fetch: fetchMock,
  }),
  getTemplateSrv: () => ({
    replace: templateReplace,
  }),
}));

const DATETIME_START = dateTime();
const DATETIME_END = dateTime().add(12, 'hour');

const FAKE_SELECT_STATEMENT = 'USE testdb; SELECT * FROM t1;';

enum TimeFieldMode {
  DISABLED,
  AUTO_GENERATE,
}

function createManualDataQuery(
  sql: string,
  manualModeAutoTimeField: TimeFieldMode = TimeFieldMode.DISABLED
): MyQuery {
  return {
    refId: '0',
    manualModeAutoTimeField: manualModeAutoTimeField === TimeFieldMode.AUTO_GENERATE,
    manualMode: true,
    manualQueryText: sql,
    builderQuerySerialized: undefined,
  };
}

function createBuilderDataQuery(sql: SelectStatement): MyQuery {
  return {
    refId: '0',
    manualModeAutoTimeField: false,
    manualMode: false,
    manualQueryText: '',
    builderQuerySerialized: sql.serialize(),
  };
}

function createDataQueryRequest(query: MyQuery, range: TimeRange): DataQueryRequest<MyQuery> {
  return {
    app: CoreApp.Dashboard,
    requestId: '0',
    intervalMs: 15000,
    scopedVars: {},
    timezone: 'local',
    startTime: DATETIME_START.unix(),
    targets: [
      {
        ...query,
        refId: '0',
      },
    ],
    range: range,
    interval: '1s',
  };
}

function generateResponse(rowCount: number, fields: string[]): string {
  let response = '';
  for (let i = 0; i < rowCount; ++i) {
    let values = fields
      .map((field) => {
        return field === 'time' ? '"time": "2022-02-02 02:02:02"' : `"${field}": ${i}`;
      })
      .join(', ');
    response += `{${values}}\r\n`;
  }
  return response;
}

describe('DataSource', () => {
  const defaultTimeRange: TimeRange = {
    from: DATETIME_START,
    to: DATETIME_END,
    raw: {
      from: DATETIME_START,
      to: DATETIME_END,
    },
  };

  let dataSource: DataSource;
  let dataQuery: MyQuery;
  let dataQueryRequest: DataQueryRequest<MyQuery>;

  beforeEach(() => {
    fetchMock.mockClear();
    dataSource = new DataSource(createDatasourceSettings(RAIJIN_URL));
    dataQuery = createManualDataQuery(FAKE_SELECT_STATEMENT);
    dataQueryRequest = createDataQueryRequest(dataQuery, defaultTimeRange);
  });

  it('should repeat 10 times on Bad GateWay and throw', async () => {
    doMockExpectedResponse(undefined, 502, 'Bad Gateway');
    await expect(dataSource.DoRequest(FAKE_SELECT_STATEMENT)).rejects.toThrow(
      'Error: Connection error: status=502; status text="Bad Gateway";'
    );
    expect(fetchMock).toHaveBeenCalledTimes(10);
  });

  it('should parse object', async () => {
    doMockExpectedResponse({ a: 0 });

    expect((await dataSource.query(dataQueryRequest)).data).toEqual([
      new MutableDataFrame({
        refId: '0',
        fields: [{ name: 'a', type: FieldType.other, values: [0] }],
      }),
    ]);
  });

  it('should throw on empty Raijin URL', () => {
    expect(() => new DataSource(createDatasourceSettings(''))).toThrow('The Raijin URL is empty');
  });

  it('should call fetch() correctly', async () => {
    doMockExpectedResponse('{"a": 0}');

    const rs = dataSource.query(dataQueryRequest);
    await expect(rs).resolves.not.toThrow();
    expect(fetchMock).toHaveBeenCalledTimes(1);
    expect(fetchMock).toHaveBeenCalledWith({
      method: 'POST',
      url: `${GRAFANA_API_URL}/raijin-proxy`,
      data: { query: FAKE_SELECT_STATEMENT },
    });
  });

  it('should throw connection error on status != 200', async () => {
    doMockExpectedResponse(undefined, 400, 'Bad Request');
    const expectedErrorText = 'Error: Connection error: status=400; status text="Bad Request";';

    const rs = dataSource.query(dataQueryRequest);
    await expect(rs).rejects.toThrow(expectedErrorText);
    expect(GetLastQueryError()).toEqual(expectedErrorText);
  });

  it('should parse raijin-exception', async () => {
    doMockExpectedResponse({
      'raijin-exception': {
        'parser-location': 'http:1:1: ',
        tag_reason: 'No database selected',
        where: 'session.cc:127',
        mode: 'debug',
        error_code: '35684484',
      },
    });

    const rs = dataSource.query(dataQueryRequest);

    const expectedErrorText = 'Error: Raijin error: No database selected';
    await expect(rs).rejects.toThrow(expectedErrorText);
    expect(GetLastQueryError()).toEqual(expectedErrorText);
  });

  it('should show raijin-exception in the connection error', async () => {
    doMockExpectedResponse(
      {
        'raijin-exception': {
          'parser-location': 'http:1:1: ',
          tag_reason: 'No database selected',
          where: 'session.cc:127',
          mode: 'debug',
          error_code: '35684484',
        },
      },
      400,
      'Bad Request'
    );

    const rs = dataSource.query(dataQueryRequest);

    const expectedErrorText =
      'Error: Connection error: status=400; status text="Bad Request"; Raijin error: No database selected';
    await expect(rs).rejects.toThrow(expectedErrorText);
    expect(GetLastQueryError()).toEqual(expectedErrorText);
  });

  it('should return empty data frame on empty SQL', async () => {
    dataQuery = createManualDataQuery('');

    const rs = await dataSource.query(createDataQueryRequest(dataQuery, defaultTimeRange));
    expect(rs.data).toHaveLength(1);
    expect(rs.data[0]).toBeInstanceOf(MutableDataFrame);
    expect(rs.data[0].fields).toHaveLength(0);
  });

  it('should return data frames', async () => {
    const fields = ['foo', 'bar'];
    const recordCount = 1234;
    doMockExpectedResponse(generateResponse(recordCount, fields));

    const rs = await dataSource.query(dataQueryRequest);

    //check result
    expect(rs).toHaveProperty('data');
    expect(rs.data).toHaveLength(1);
    expect(rs.data[0]).toBeInstanceOf(MutableDataFrame);
    expect(rs.data[0].fields).toHaveLength(fields.length);

    // check that result contains all field names
    rs.data[0].fields.forEach((field: MutableField) => {
      expect(fields).toContain(field.name);
    });

    // check values
    rs.data[0].fields.forEach((field: MutableField) => {
      expect(field.values.length).toEqual(recordCount);
      field.values.toArray().forEach((value, index) => {
        expect(value).toEqual(index);
      });
    });
  });

  it('should not return time field in manual mode (manualModeAutoTimeField is off)', async () => {
    doMockExpectedResponse('{"a": 123, "b": 123}');

    const rs = await dataSource.query(dataQueryRequest);
    rs.data[0].fields.forEach((field: MutableField) => {
      expect(field.name).not.toEqual('time');
    });
  });

  it('should return time field in manual mode (manualModeAutoTimeField is on)', async () => {
    dataQuery = createManualDataQuery(FAKE_SELECT_STATEMENT, TimeFieldMode.AUTO_GENERATE);
    doMockExpectedResponse('{"a": 123, "b": 123}');

    const rs = await dataSource.query(createDataQueryRequest(dataQuery, defaultTimeRange));
    const receivedFields = rs.data[0].fields.map((field: MutableField) => field.name);
    expect(receivedFields).toContain('time');
  });

  it('should override time field in manual mode (manualModeAutoTimeField is on)', async () => {
    dataQuery = createManualDataQuery(FAKE_SELECT_STATEMENT, TimeFieldMode.AUTO_GENERATE);
    doMockExpectedResponse('{"a": 123, "time": ""}');

    const rs = await dataSource.query(createDataQueryRequest(dataQuery, defaultTimeRange));
    const receivedFields: string[] = rs.data[0].fields.map((field: MutableField) => field.name);
    expect(receivedFields).toContain('time');
    const timeFieldsCount: number = receivedFields.reduce(
      (count: number, fieldName: string) => count + (fieldName === 'time' ? 1 : 0),
      0
    );
    expect(timeFieldsCount).toEqual(1);
  });

  it('should process exception in the middle of data', async () => {
    doMockExpectedResponse(
      '{"time": "2022-10-02 00:00:00", "a": 0, "b": 1 }\r\n' +
        '{"time": "2022-10-02 01:01:01", "a": 1, "b": 1 }\r\n' +
        '{"raijin-exception": {"parser-location": "http:1:54: ", "tag_reason": "Evaluation error (invalid data in 1 rows) in (CAST_INT32_TO_DOUBLE(a) /. CAST_INT32_TO_DOUBLE(b))", "where": "resultstream.cc:14", "mode": "debug", "error_code": "130"}}\r\n' +
        '{"time": "2022-10-02 02:02:02", "a": 1, "b": 2 }'
    );

    const rs = dataSource.query(dataQueryRequest);

    const expectedErrorText =
      'Evaluation error (invalid data in 1 rows) in (CAST_INT32_TO_DOUBLE(a) /. CAST_INT32_TO_DOUBLE(b))';
    await expect(rs).rejects.toThrow(expectedErrorText);
    expect(GetLastQueryError()).toEqual(expectedErrorText);
  });

  function createBuilderSelectStatement(fields: string[], timeColumn: TimeColumn): SelectStatement {
    let columns = new ColumnList();
    fields.forEach((field) => {
      columns.add(new Column(undefined, '', new DbField('testdb', 'tbl', field)));
    });
    return new SelectStatement(
      new FromClause('testdb', 'tbl'),
      columns,
      undefined, // where
      undefined, // group by
      undefined, // having
      timeColumn
    );
  }

  it('should process the builder query', async () => {
    const disabledTimeColumn = new TimeColumn(false);
    const fields = ['a', 'b', 'c'];
    const selectStatement = createBuilderSelectStatement(fields, disabledTimeColumn);
    const dataQuery = createBuilderDataQuery(selectStatement);

    const rowCount = 1245;
    doMockExpectedResponse(generateResponse(rowCount, fields));

    const rs = dataSource.query(createDataQueryRequest(dataQuery, defaultTimeRange));
    await expect(rs).resolves.not.toThrow();
    expect(fetchMock).toHaveBeenCalledWith({
      method: 'POST',
      url: `${GRAFANA_API_URL}/raijin-proxy`,
      data: { query: selectStatement.getSql() },
    });

    expect((await rs).data[0].fields).toHaveLength(fields.length);
    for (const field of fields) {
      const index = fields.indexOf(field);
      expect((await rs).data[0].fields[index].name).toEqual(field);

      const receivedValues = (await rs).data[0].fields[index].values.toArray();
      expect(receivedValues).toHaveLength(rowCount);

      for (let i = 0; i < rowCount; ++i) {
        expect(receivedValues[i]).toEqual(i);
      }
    }
  });

  it('should process time column in the builder query', async () => {
    const timeColumn = new TimeColumn(
      true,
      false,
      new DbField('testdb', 'tbl', 'time', 'datetime')
    );
    let fields = ['a', 'b'];
    const selectStatement = createBuilderSelectStatement(fields, timeColumn);
    const query = createBuilderDataQuery(selectStatement);

    // should return a time column from raijin
    fields.push('time');
    doMockExpectedResponse(generateResponse(1, fields));

    const rs = await dataSource.query(createDataQueryRequest(query, defaultTimeRange));

    // should have a time column
    const hasTimeColumn = rs.data[0].fields.find((field: MutableField) => field.name === 'time');
    expect(hasTimeColumn).toBeDefined();
    // should not add auto time field
    const timeColumnsCount = rs.data[0].fields.reduce(
      (count: number, field: MutableField) => count + (field.name === 'time' ? 1 : 0),
      0
    );
    expect(timeColumnsCount).toEqual(1);

    // should have the WHERE clause with given time range
    const timeColumnName = `"tbl"."time"`;
    const expectedWhereStatement = `\nWHERE\n\t${timeColumnName} >= '${defaultTimeRange.from.toISOString()}' AND ${timeColumnName} <= '${defaultTimeRange.to.toISOString()}'`;
    expect(fetchMock).toHaveBeenCalledWith({
      method: 'POST',
      url: `${GRAFANA_API_URL}/raijin-proxy`,
      data: { query: selectStatement.getSql() + expectedWhereStatement },
    });
  });

  it('should auto generate the time column in builder mode', async () => {
    const timeColumn = new TimeColumn(true, true); // autogenerated time column
    // the time column should be overridden by the DataSource.query() method afterwards
    const fields = ['a', 'b', 'c', 'time'];
    const selectStatement = createBuilderSelectStatement(fields, timeColumn);
    const query = createBuilderDataQuery(selectStatement);

    const recordCount = 4289;
    doMockExpectedResponse(generateResponse(recordCount, fields));

    const rs = await dataSource.query(createDataQueryRequest(query, defaultTimeRange));

    const autogeneratedTimeColumn = rs.data[0].fields.find(
      (field: MutableField) => field.name === 'time'
    );
    expect(autogeneratedTimeColumn).toBeDefined();

    const localDatetimeStart = dateTime(DATETIME_START).add(100, 'millisecond'); // the same as in TimeValuesGenerator.constructor
    const step = (DATETIME_END.valueOf() - DATETIME_START.valueOf()) / (recordCount - 1);
    let dt = localDatetimeStart.valueOf();
    const values: MutableVector = (autogeneratedTimeColumn as MutableField).values;
    values.toArray().forEach((value) => {
      expect(value).toEqual(dt);
      dt += step;
    });
  });
});

describe('DataSource.testDatasource', () => {
  let dataSource: DataSource;

  beforeEach(() => {
    fetchMock.mockClear();
    dataSource = new DataSource(createDatasourceSettings(RAIJIN_URL));
  });

  it('should return success', async () => {
    fetchMock.mockImplementation((_options: BackendSrvRequest) => {
      return of(createRaijinServerResponse('{"a": 0}'));
    });
    await expect(dataSource.testDatasource()).resolves.not.toThrow();
    await expect(dataSource.testDatasource()).resolves.toEqual({
      status: 'success',
      message: 'Connection successful',
    });
  });

  it('should process connection errors', async () => {
    fetchMock.mockImplementation((_options: BackendSrvRequest) => {
      return of(createRaijinServerResponse(undefined, 400, 'Bad Request'));
    });
    await expect(dataSource.testDatasource()).rejects.toThrow(
      'Error: Connection error: status=400; status text="Bad Request";'
    );
  });
});

describe('DataSource.metricFindQuery', () => {
  let dataSource: DataSource;

  beforeEach(() => {
    fetchMock.mockClear();
    dataSource = new DataSource(createDatasourceSettings(RAIJIN_URL));
  });

  it('should throw if there are too many fields in the query', async () => {
    const fields = ['a', 'b', 'c'];
    doMockExpectedResponse(generateResponse(1, fields));

    const rs = dataSource.metricFindQuery(FAKE_SELECT_STATEMENT);
    const expectedError = `Wrong number of fields in var query (fields count='${fields.length}')`;
    await expect(rs).rejects.toThrow(expectedError);
  });

  it('should return empty array on no data', async () => {
    doMockExpectedResponse('');

    const rs = dataSource.metricFindQuery(FAKE_SELECT_STATEMENT);
    await expect(rs).resolves.toHaveLength(0);
  });

  it('should return array of MetricFindValue', async () => {
    const rowCount = 100;
    doMockExpectedResponse(generateResponse(rowCount, ['a']));

    const rs: MetricFindValue[] = await dataSource.metricFindQuery(FAKE_SELECT_STATEMENT);
    expect(rs).toHaveLength(rowCount);
    let expectedValue = 0;
    rs.forEach((metricFindValue: MetricFindValue) => {
      expect(metricFindValue.text).toEqual(expectedValue);
      ++expectedValue;
    });
  });

  it('should process raijin-exception', async () => {
    doMockExpectedResponse(
      '{ "a": 0 }\r\n' +
        '{ "a": 1 }\r\n' +
        '{"raijin-exception": {"parser-location": "http:1:54: ", "tag_reason": "Evaluation error (invalid data in 1 rows) in (CAST_INT32_TO_DOUBLE(a) /. CAST_INT32_TO_DOUBLE(b))", "where": "resultstream.cc:14", "mode": "debug", "error_code": "130"}}'
    );

    const rs = dataSource.metricFindQuery(FAKE_SELECT_STATEMENT);

    const expectedErrorText =
      'Evaluation error (invalid data in 1 rows) in (CAST_INT32_TO_DOUBLE(a) /. CAST_INT32_TO_DOUBLE(b))';
    await expect(rs).rejects.toThrow(expectedErrorText);
  });
});
