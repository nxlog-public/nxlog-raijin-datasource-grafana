import { DataSource } from './datasource';

export class DbField {
  constructor(
    readonly databaseName: string = '',
    readonly tableName: string = '',
    readonly name: string = '',
    readonly type: string = ''
  ) {}

  getSql(): string {
    return `"${this.tableName}"."${this.name}"`;
  }
  equalsTo(other: DbField): boolean {
    return (
      this.databaseName === other.databaseName &&
      this.tableName === other.tableName &&
      this.name === other.name &&
      this.type === other.type
    );
  }
  isAsterisk(): boolean {
    return this.name === '*';
  }
  static createAsteriskField(databaseName: string, tableName: string): DbField {
    return new DbField(databaseName, tableName, '*');
  }
}

export class DatabaseClient {
  constructor(datasource: DataSource) {
    this.datasource = datasource;
  }
  async getDatabasesList(): Promise<string[]> {
    const response: string = await this.datasource.DoRequest('SHOW DATABASES');
    const lines = String(response)
      .split('\r\n')
      .filter((line) => line !== '');
    let databases: string[] = [];
    for (const line of lines) {
      const databaseObj = JSON.parse(line.trim());
      databases.push(databaseObj.database);
    }
    return databases;
  }

  async getTablesList(database: string): Promise<string[]> {
    if (database === '') {
      return [];
    }
    const response: string = await this.datasource.DoRequest(`USE "${database}"; SHOW TABLES;`);
    const lines = String(response)
      .split('\r\n')
      .filter((line) => line !== '');
    let tables: string[] = [];
    for (const line of lines) {
      const tableObj = JSON.parse(line.trim());
      tables.push(tableObj.table);
    }
    return tables;
  }

  async getFieldsList(database: string, table: string): Promise<DbField[]> {
    if (database === '' || table === '') {
      return [];
    }
    const response = await this.datasource.DoRequest(
      `USE "${database}"; SELECT FIELDS FROM "${table}"`
    );
    const lines = String(response)
      .split('\r\n')
      .filter((line) => line !== '');
    let fields: DbField[] = [];
    for (const line of lines) {
      const fieldObj = JSON.parse(line.trim());
      fields.push(new DbField(database, table, fieldObj.field, fieldObj.type));
    }
    return fields;
  }

  private datasource: DataSource;
}
