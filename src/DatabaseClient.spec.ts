import { DataSource } from './datasource';
import { DatabaseClient, DbField } from './DatabaseClient';
import {
  createDatasourceSettings,
  doMockExpectedResponse,
  fetchMock,
  GRAFANA_API_URL,
  RAIJIN_URL,
} from './testcommon';

jest.mock('@grafana/runtime', () => ({
  ...jest.requireActual('@grafana/runtime'),
  getBackendSrv: () => ({
    fetch: fetchMock,
  }),
}));

describe('DatabaseClient', () => {
  let dataSource: DataSource;

  beforeEach(() => {
    fetchMock.mockClear();
    dataSource = new DataSource(createDatasourceSettings(RAIJIN_URL));
  });

  it('should return database list', async () => {
    doMockExpectedResponse(
      '{"database":"testdb1","size":"123 bytes"}\r\n' +
        '{"database":"testdb2","size":"234 bytes"}\r\n' +
        '{"database":"testdb3","size":"345 bytes"}\r\n'
    );

    const dbClient = new DatabaseClient(dataSource);

    const databaseList = await dbClient.getDatabasesList();
    expect(databaseList).toHaveLength(3);
    expect(databaseList).toEqual(['testdb1', 'testdb2', 'testdb3']);

    expect(fetchMock).toBeCalledWith({
      data: {
        query: 'SHOW DATABASES',
      },
      method: 'POST',
      url: `${GRAFANA_API_URL}/raijin-proxy`,
    });
  });

  it('should return table list', async () => {
    doMockExpectedResponse(
      '{"table":"tbl1","record_count":"123","encrypted":"yes","partitioning_type":"none","size":"1123 bytes"}\r\n' +
        '{"table":"tbl2","record_count":"12","encrypted":"yes","partitioning_type":"none","size":"123 bytes"}\r\n' +
        '{"table":"tbl3","record_count":"23","encrypted":"no","partitioning_type":"expression","size":"123 bytes"}\r\n' +
        '{"table":"tbl4","record_count":"0","encrypted":"yes","partitioning_type":"name","size":"3 bytes"}\r\n'
    );

    const dbClient = new DatabaseClient(dataSource);
    await expect(dbClient.getTablesList('')).resolves.toEqual([]);

    const tableList = await dbClient.getTablesList('testdb1');
    expect(tableList).toHaveLength(4);
    expect(tableList).toContain('tbl1');
    expect(tableList).toContain('tbl2');
    expect(tableList).toContain('tbl3');
    expect(tableList).toContain('tbl4');

    expect(fetchMock).toBeCalledWith({
      data: {
        query: 'USE "testdb1"; SHOW TABLES;',
      },
      method: 'POST',
      url: `${GRAFANA_API_URL}/raijin-proxy`,
    });
  });

  it('should return fields list', async () => {
    doMockExpectedResponse(
      '{"field":"a","type":"INT"}\r\n{"field":"b","type":"BIGINT"}\r\n{"field":"dt","type":"DATETIME"}\r\n'
    );

    const dbClient = new DatabaseClient(dataSource);

    await expect(dbClient.getFieldsList('', 'tbl1')).resolves.toEqual([]);
    await expect(dbClient.getFieldsList('testdb', '')).resolves.toEqual([]);

    const fieldList = await dbClient.getFieldsList('testdb1', 'tbl2');
    expect(fieldList).toHaveLength(3);
    expect(fieldList).toContainEqual(new DbField('testdb1', 'tbl2', 'a', 'INT'));
    expect(fieldList).toContainEqual(new DbField('testdb1', 'tbl2', 'b', 'BIGINT'));
    expect(fieldList).toContainEqual(new DbField('testdb1', 'tbl2', 'dt', 'DATETIME'));

    expect(fetchMock).toBeCalledWith({
      data: {
        query: 'USE "testdb1"; SELECT FIELDS FROM "tbl2"',
      },
      method: 'POST',
      url: `${GRAFANA_API_URL}/raijin-proxy`,
    });
  });
});

describe('DbField', () => {
  it('should generate sql', () => {
    const field_a = new DbField('testdb1', 'tbl2', 'a', 'INT');
    const field_b = new DbField('testdb1', 'tbl2', 'b', 'BIGINT');
    const field_dt = new DbField('testdb1', 'tbl2', 'dt', 'DATETIME');

    expect(field_a.getSql()).toEqual('"tbl2"."a"');
    expect(field_b.getSql()).toEqual('"tbl2"."b"');
    expect(field_dt.getSql()).toEqual('"tbl2"."dt"');
  });

  it('should be equal to', () => {
    const field_1 = new DbField('testdb1', 'tbl2', 'a', 'INT');
    const field_2 = new DbField('testdb1', 'tbl2', 'a', 'INT');

    expect(field_1.equalsTo(field_2)).toEqual(true);

    const field_3 = new DbField('testdb1', 'tbl1', 'a', 'INT');
    const field_4 = new DbField('testdb2', 'tbl2', 'a', 'INT');
    const field_5 = new DbField('testdb1', 'tbl2', 'b', 'INT');
    const field_6 = new DbField('testdb1', 'tbl2', 'a', 'BIGINT');

    expect(field_1.equalsTo(field_3)).toEqual(false);
    expect(field_1.equalsTo(field_4)).toEqual(false);
    expect(field_1.equalsTo(field_5)).toEqual(false);
    expect(field_1.equalsTo(field_6)).toEqual(false);
  });

  it('should process asterisks', () => {
    const field = DbField.createAsteriskField('testdb1', 'tbl1');

    expect(field.isAsterisk()).toEqual(true);
    expect(field.getSql()).toEqual('"tbl1"."*"');
    expect(field.type).toEqual('');
  });
});
