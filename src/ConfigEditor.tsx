import React, { ChangeEvent, PureComponent } from 'react';
import { Icon, LegacyForms, Select, Alert } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps, SelectableValue } from '@grafana/data';
import { MyDataSourceOptions } from './types';

const { FormField } = LegacyForms;

interface Props extends DataSourcePluginOptionsEditorProps<MyDataSourceOptions> {}

interface State {
  accessHelpIsVisible: boolean;
}

const kAccessOptions: Array<SelectableValue<string>> = [
  {
    label: 'Server (default)',
    value: 'proxy',
  },
  {
    label: 'Browser',
    value: 'direct',
  },
];

const kDefaultAccessOption: SelectableValue<string> = {
  label: 'Server (default)',
  value: 'proxy',
};

const UrlAccessTooltip = (access: string) => {
  switch (access) {
    case 'proxy':
      return (
        <>
          Your access method is <em>Server</em>, this means the Raijin needs to be accessible from
          the grafana backend/server (probably, via a private network).
        </>
      );
    case 'direct':
      return (
        <>
          Your access method is <em>Browser</em>, this means the Raijin needs to be accessible from
          the browser (via public network).
        </>
      );
    default:
      throw new Error('Wrong access value: ' + access);
  }
};

const HttpAccessHelp = () => (
  <Alert severity="info" title="Access mode">
    <p>
      Access mode controls how requests to the data source will be handled.
      <strong>
        &nbsp;<i>Server</i>
      </strong>
      should be the preferred way if nothing else is stated.
    </p>
    <div className="alert-title">Server access mode (Default):</div>
    <p>
      All requests will be made from the browser to Grafana backend/server which in turn will
      forward the requests to the data source and by that circumvent possible Cross-Origin Resource
      Sharing (CORS) requirements. The URL needs to be accessible from the grafana backend/server if
      you select this access mode.
    </p>
    <div className="alert-title">Browser access mode:</div>
    <p>
      All requests will be made from the browser directly to the data source and may be subject to
      Cross-Origin Resource Sharing (CORS) requirements. The URL needs to be accessible from the
      browser if you select this access mode.
    </p>
  </Alert>
);

export class ConfigEditor extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      accessHelpIsVisible: false,
    };
  }

  onUrlChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    options.url = event.target.value;
    options.jsonData.directUrl = event.target.value;
    onOptionsChange({ ...options, ...options.jsonData });
  };

  onAccessChange = (value: SelectableValue<string>) => {
    const { onOptionsChange, options } = this.props;
    options.access = value.value!;
    if (options.access === 'direct') {
      options.url = options.jsonData.directUrl;
    }
    onOptionsChange({ ...options, ...options.jsonData });
  };

  override render() {
    const { options } = this.props;

    const accessSelect = (
      <Select<string>
        isSearchable={false}
        width={24}
        options={kAccessOptions}
        value={kAccessOptions.filter((o) => o.value === options.access)[0] || kDefaultAccessOption}
        onChange={this.onAccessChange}
        disabled={true}
      />
    );

    return (
      <div className="gf-form-group">
        <>
          <div className="gf-form">
            <div className="gf-form-inline">
              <div className="gf-form">
                <FormField
                  label="Access"
                  labelWidth={8}
                  inputEl={accessSelect}
                  tooltip={'Only the server mode is available currently'}
                />
              </div>
              <div className="gf-form">
                {/* NOTE(pavel.yurin@nxlog.org): pay attention, the label component below is
                      a "standard html" label and not the Label from grafana.ui (the latter behaves
                       wierd in this concrete case) */}
                <label
                  className="gf-form-label query-keyword pointer"
                  onClick={() =>
                    this.setState({ accessHelpIsVisible: !this.state.accessHelpIsVisible })
                  }
                >
                  Help&nbsp;
                  <Icon
                    name={this.state.accessHelpIsVisible ? 'angle-down' : 'angle-right'}
                    style={{ marginBottom: 0 }}
                  />
                </label>
              </div>
            </div>
          </div>
          {this.state.accessHelpIsVisible && <HttpAccessHelp />}
        </>
        <div className="gf-form">
          <FormField
            label="URL"
            labelWidth={8}
            inputWidth={60}
            onChange={this.onUrlChange}
            tooltip={UrlAccessTooltip(options.access)}
            value={options.url || options.jsonData.directUrl || ''}
            placeholder="localhost:2500"
            required={true}
          />
        </div>
      </div>
    );
  }
}
