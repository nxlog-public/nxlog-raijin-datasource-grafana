import { DataQuery, DataSourceJsonData } from '@grafana/data';
import { SelectStatementDetails } from './query-editor/sql-statement/SelectStatement';

export interface MyQuery extends DataQuery {
  manualMode: boolean;
  manualQueryText: string;
  manualModeAutoTimeField: boolean;
  builderQuerySerialized?: SelectStatementDetails;
}

export const defaultQuery: Partial<MyQuery> = {
  manualMode: false,
  manualModeAutoTimeField: false,
  manualQueryText: '',
};

/**
 * These are options configured for each DataSource instance
 */
export interface MyDataSourceOptions extends DataSourceJsonData {
  directUrl: string;
}
