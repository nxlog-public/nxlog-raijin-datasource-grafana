Dependencies:

- node v16
- yarn

The script will attempt use nvm.sh (if available) to select node v16.

To start the build run the script from the repostory root:

```
cd raijin
plugins/external/grafana/build.sh
```

The script exits early if dependencies are not found or it's in the wrong directory.
The build will run in `./build-plugin-grafana`
It will remove and recreate the build directory as it starts building.

