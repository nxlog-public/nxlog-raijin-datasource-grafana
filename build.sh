#!/usr/bin/env bash

BUILDDIR=build-plugin-grafana
PLUGINDIR=plugins/external/grafana
# versions above 16 do not work
NODEREQ=16
# the name of the plugin
NAME=nxlog-raijin-datasource

if [ ! -d "./$PLUGINDIR" ] ; then 
   echo Please run this from the raijin repository root
   exit 1
fi

pre_check(){
   ERR=0
   # Try to use nvm to set the correct node version
   # But do not require it for building.
   . ~/.nvm/nvm.sh && nvm use $NODEREQ
   NODEVERSION=$(node -v)
   YARNVERSION=$(yarn -v)
   if echo $NODEVERSION | grep -Eq '^v'$NODEREQ'\.'; then
      echo "Node version found: $NODEVERSION"
   else
      echo "Node version 16 is required. Found: $NODEVERSION"
      ERR=1
   fi
   if [ "$NODEVERSION" == "" ] ; then
      echo "Node executable was not found"
      ERR=1
   fi
   if [ "$YARNVERSION" == "" ] ; then 
      echo "Yarn executable was not found"
      ERR=1
   fi
   if [ $ERR == 1 ]; then
      echo "Dependency checks failed."
      exit 1
   fi
}

build_grafana_plugin(){
   [ -d "./$BUILDDIR" ] && rm -fr "./$BUILDDIR"
   mkdir -p $BUILDDIR
   echo Building in $BUILDDIR
   # Grafana plugin build
   cp -r $PLUGINDIR $BUILDDIR
   cd $BUILDDIR/$(basename $PLUGINDIR)
   yarn install --frozen-filelock
   yarn build
   mv dist $NAME
   zip --quiet -r ../$NAME.zip $NAME
   cd ..
   md5sum $NAME.zip > $NAME.zip.md5 
   echo "Built $(pwd)/$NAME.zip"
}

pre_check
build_grafana_plugin
